/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.presentation.tests;

import static org.junit.Assert.assertFalse;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.knowhowlab.osgi.testing.assertions.BundleAssert;
import org.knowhowlab.osgi.testing.utils.BundleUtils;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.presentation.DelegatingConverterFactory;
import org.eclipse.osbp.ecview.core.common.presentation.IConverterFactory;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.internal.TextFieldPresentation;
import org.eclipse.osbp.ui.api.functionlibrary.IFunctionLibraryService;
import org.eclipse.osbp.utils.functionnormalizer.api.FunctionTypingAPI;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;

import org.eclipse.osbp.ecview.extension.model.converter.YCustomDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.util.SimpleECviewExtensionModelFactory;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.converter.CustomDecimalConverter;
import org.eclipse.osbp.ecview.extension.vaadin.components.utils.FunctionWrapper;

/**
 * Tests the {@link TextFieldPresentation}.
 */
@SuppressWarnings("restriction")
public class FunctionTests {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(FunctionTests.class);
	private final DecimalFormat defaultDecimalFormat = new DecimalFormat(
			"##,##0.00");

	private SimpleECviewExtensionModelFactory factory = new SimpleECviewExtensionModelFactory();
	private CssLayout rootLayout = new CssLayout();
	private EMap<String, String> properties = new BasicEMap();
	private BundleContext bc;

	protected static String CUSTOM_DECIMAL_CONVERTER = "CUSTOM_DECIMAL_CONVERTER";

	/**
	 * Setup tests.
	 * 
	 * @throws ConfigurationException
	 * @throws BundleException
	 */
	@Before
	public void setup() throws ConfigurationException, BundleException {
		UI.setCurrent(new DefaultUI());
		UI.getCurrent().setContent(rootLayout);

		bc = FrameworkUtil.getBundle(getClass()).getBundleContext();

		BundleUtils.startBundleAsync(bc,
				"org.eclipse.osbp.xtext.functionlibrarydsl.provider");

		properties
				.put("functionToModelFormat",
						"org.eclipse.osbp.ecview.extension.presentation.tests.utils.Formatter.lengthToModelConverter");
		properties.put("functionBaseUnit", "cm");
	}

	@After
	public void after() {
		DelegatingConverterFactory.getInstance().clear();
	}

	/**
	 * Tests rendering issues.
	 * 
	 * @throws Exception
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void convertLengthStringToDouble() throws Exception {
		// END SUPRESS CATCH EXCEPTION
		// build the view model
		// ...> yView
		// ......> yGridLayout
		// .........> yText
		// YView yView = factory.createView();
		// YGridLayout yGridlayout = factory.createGridLayout();
		// yView.setContent(yGridlayout);
		// YDecimalField yDecimal = factory.createDecimalField();
		// YCustomDecimalConverter yConverter =
		// YConverterFactory.eINSTANCE.createYCustomDecimalConverter();
		// yDecimal.setConverter(yConverter);
		// yGridlayout.getElements().add(yDecimal);
		//
		// DelegatingConverterFactory.getInstance().addDelegate(
		// new ConverterFactory(CUSTOM_DECIMAL_CONVERTER));
		//
		// VaadinRenderer renderer = new VaadinRenderer();
		// renderer.render(rootLayout, yView, null);
		//
		// IDecimalFieldEditpart decimalEditpart = DelegatingEditPartManager
		// .getInstance().getEditpart(yDecimal);
		// IWidgetPresentation<Component> presentation = decimalEditpart
		// .getPresentation();
		// DecimalField decimal = (DecimalField) presentation.getWidget();
		//
		// yDecimal.setValue(10);
		// assertEquals(10, yDecimal.getValue(), 0);
		// assertEquals("10,00 cm", decimal.getValue());
		//
		// decimal.setValue("10,00 cm");
		// assertEquals(10, yDecimal.getValue(), 0);
		// assertEquals("10,00 cm", decimal.getValue());
		//

		BundleAssert.assertBundleState(Bundle.ACTIVE,
				"org.eclipse.osbp.xtext.functionlibrarydsl.provider");

		String value = "10,00 cm";
		Locale locale = Locale.GERMANY;

		Number n;
		try {
			n = defaultDecimalFormat.parse(value);
			Double dbl = new Double(n.doubleValue());
			String dblStr = defaultDecimalFormat.format(dbl);
			int idx = dblStr.toString().length();
			String suffix = value.substring(idx).trim();
			String baseSuffix = null;
			FunctionTypingAPI functionTypingAPI = new FunctionTypingAPI();
//			for (Entry<String, String> entry : getProperties()) {
//				if (functionTypingAPI.getFunctionBaseUnitTypeName().equals(
//						entry.getKey())) {
//					baseSuffix = entry.getValue();
//				}
//			}
			for (Entry<String, String> entry : getProperties()) {
				if (functionTypingAPI.getFunctionConverterTypeName()
						.equals(entry.getKey())) {
					n = handleToModelFormat(entry.getValue(), n.doubleValue(),
							locale, baseSuffix, suffix);
				}
			}
		} catch (ParseException e) {
			assertFalse(true);
		}

	}

	private static class ConverterFactory implements IConverterFactory {

		private String converterName;

		public ConverterFactory(String converterName) {
			super();
			this.converterName = converterName;
		}

		public ConverterFactory() {
			super();
		}

		@Override
		public boolean isFor(IViewContext uiContext, IConverterEditpart editpart) {
			return true;
		}

		@Override
		public Object createConverter(IViewContext uiContext,
				IConverterEditpart editpart) throws IllegalArgumentException {
			if (FunctionTests.CUSTOM_DECIMAL_CONVERTER.equals(converterName)) {
				return new CustomDecimalConverter(uiContext,
						(YCustomDecimalConverter) editpart.getModel());
			}
			return new TextToTextConverter();
		}
	}

	/**
	 * Converters "Test" to "Test $" and "Test $" to "Test"
	 */
	@SuppressWarnings("serial")
	private static class TextToTextConverter implements
			Converter<String, String> {

		@Override
		public String convertToModel(String value,
				Class<? extends String> targetType, Locale locale)
				throws com.vaadin.data.util.converter.Converter.ConversionException {
			return value == null ? "" : value.replaceAll(" XX", "");
		}

		@Override
		public String convertToPresentation(String value,
				Class<? extends String> targetType, Locale locale)
				throws com.vaadin.data.util.converter.Converter.ConversionException {
			return value == null ? " XX" : value + " XX";
		}

		@Override
		public Class<String> getModelType() {
			return String.class;
		}

		@Override
		public Class<String> getPresentationType() {
			return String.class;
		}

	}

	// HELPER METHODS

	private Double handleToModelFormat(String functionFqn, Double value,
			Locale locale, String baseSuffix, String suffix) {

		ServiceReference<IFunctionLibraryService> ref = bc
				.getServiceReference(IFunctionLibraryService.class);
		IFunctionLibraryService service = bc.getService(ref);

		FunctionWrapper wrapper = new FunctionWrapper(functionFqn);
		try {
			if (locale != null) {
				Double callResult = (Double) service.invoke(
						wrapper.getClassName(), wrapper.getMethodName(), value,
						locale, baseSuffix, suffix);
				return callResult;
			}
		} catch (ClassCastException ex) {
			// TODO (JCD): Translation
			LOGGER.error("Return type of the function call '" + functionFqn
					+ "' is not a " + Double.class.getSimpleName() + "!");
		}
		return value;
	}

	// GETTER
	public EMap<String, String> getProperties() {
		return properties;
	}

}
