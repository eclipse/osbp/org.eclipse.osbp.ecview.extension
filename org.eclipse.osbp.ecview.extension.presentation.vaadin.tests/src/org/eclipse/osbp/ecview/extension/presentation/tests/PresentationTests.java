/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.presentation.tests;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YDelegateConverter;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.presentation.DelegatingConverterFactory;
import org.eclipse.osbp.ecview.core.common.presentation.IConverterFactory;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDecimalField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextField;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.IDecimalFieldEditpart;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.INumericFieldEditpart;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.ITextFieldEditpart;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.IConstants;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.VaadinRenderer;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.internal.TextFieldPresentation;
import org.eclipse.osbp.runtime.web.vaadin.components.fields.DecimalField;
import org.eclipse.osbp.runtime.web.vaadin.components.fields.NumericField;
import org.osgi.framework.BundleException;
import org.osgi.service.cm.ConfigurationException;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

import org.eclipse.osbp.ecview.extension.editparts.components.ICustomDecimalFieldEditpart;
import org.eclipse.osbp.ecview.extension.model.YCustomDecimalField;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterFactory;
import org.eclipse.osbp.ecview.extension.model.converter.YDecimalToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.util.SimpleECviewExtensionModelFactory;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.converter.DecimalToUomoConverter;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.converter.NumericToUomoConverter;
import org.eclipse.osbp.ecview.extension.vaadin.components.CustomDecimalField;

/**
 * Tests the {@link TextFieldPresentation}.
 */
@SuppressWarnings("restriction")
public class PresentationTests {

	private SimpleECviewExtensionModelFactory factory = new SimpleECviewExtensionModelFactory();
	private CssLayout rootLayout = new CssLayout();
	
	protected static String NUMBER_TO_UOMO_CONVERTER = "NUMBER_TO_UOMO_CONVERTER";
	protected static String DECIMAL_TO_UOMO_CONVERTER = "DECIMAL_TO_UOMO_CONVERTER";

	/**
	 * Setup tests.
	 * 
	 * @throws ConfigurationException
	 * @throws BundleException
	 */
	@Before
	public void setup() throws ConfigurationException, BundleException {
		UI.setCurrent(new DefaultUI());
		UI.getCurrent().setContent(rootLayout);
	}

	@After
	public void after() {
		DelegatingConverterFactory.getInstance().clear();
	}

	/**
	 * Tests rendering issues.
	 * 
	 * @throws Exception
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_DecimalCustomEnd() throws Exception {
		// END SUPRESS CATCH EXCEPTION
		// build the view model
		// ...> yView
		// ......> yGridLayout
		// .........> yText
		YView yView = factory.createView();
		YGridLayout yGridlayout = factory.createGridLayout();
		yView.setContent(yGridlayout);
		YCustomDecimalField yCustomDecimal = factory.createCustomDecimalField();
		yGridlayout.getElements().add(yCustomDecimal);
	
		VaadinRenderer renderer = new VaadinRenderer();
		IViewContext context = renderer.render(rootLayout, yView, null);
	
		ICustomDecimalFieldEditpart customDecimalEditpart = DelegatingEditPartManager
				.getInstance().getEditpart(context, yCustomDecimal);
		IWidgetPresentation<Component> presentation = customDecimalEditpart
				.getPresentation();
		CustomDecimalField customDecimal = (CustomDecimalField) presentation.getWidget();
	
		String PRICE_END_VALUE = "cx_price_end_value";
		yCustomDecimal.setValue(10.98);
		assertEquals(IConstants.CSS_CLASS_CONTROL + " " + PRICE_END_VALUE, customDecimal.getStyleName());
		yCustomDecimal.setValue(10.99);
		assertEquals(IConstants.CSS_CLASS_CONTROL, customDecimal.getStyleName());
	}


	/**
	 * Tests rendering issues.
	 * 
	 * @throws Exception
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_TextToTextConverter() throws Exception {
		// END SUPRESS CATCH EXCEPTION
		// build the view model
		// ...> yView
		// ......> yGridLayout
		// .........> yText
		YView yView = factory.createView();
		YGridLayout yGridlayout = factory.createGridLayout();
		yView.setContent(yGridlayout);
		YTextField yText = factory.createTextField();
		YDelegateConverter yConverter = CoreModelFactory.eINSTANCE
				.createYDelegateConverter();
		yConverter.setConverterId("text.test");
		yText.setConverter(yConverter);
		yGridlayout.getElements().add(yText);

		DelegatingConverterFactory.getInstance().addDelegate(
				new ConverterFactory());

		VaadinRenderer renderer = new VaadinRenderer();
		IViewContext context = renderer.render(rootLayout, yView, null);

		ITextFieldEditpart textEditpart = DelegatingEditPartManager
				.getInstance().getEditpart(context, yText);
		IWidgetPresentation<Component> presentation = textEditpart
				.getPresentation();
		TextField text = (TextField) presentation.getWidget();

		yText.setValue("Huhu");
		assertEquals("Huhu", yText.getValue());
		assertEquals("Huhu XX", text.getValue());

		text.setValue("Haha XX");
		assertEquals("Haha", yText.getValue());
		assertEquals("Haha XX", text.getValue());
	}

	/**
	 * Tests rendering issues.
	 * 
	 * @throws Exception
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_DecimalToUomoConverter() throws Exception {
		// END SUPRESS CATCH EXCEPTION
		// build the view model
		// ...> yView
		// ......> yGridLayout
		// .........> yText
		YView yView = factory.createView();
		YGridLayout yGridlayout = factory.createGridLayout();
		yView.setContent(yGridlayout);
		YDecimalField yDecimal = factory.createDecimalField();
		YDecimalToUomoConverter yConverter = YConverterFactory.eINSTANCE.createYDecimalToUomoConverter();
		yDecimal.setConverter(yConverter);
		yGridlayout.getElements().add(yDecimal);
	
		DelegatingConverterFactory.getInstance().addDelegate(
				new ConverterFactory(DECIMAL_TO_UOMO_CONVERTER));
	
		VaadinRenderer renderer = new VaadinRenderer();
		IViewContext context = renderer.render(rootLayout, yView, null);
	
		IDecimalFieldEditpart decimalEditpart = DelegatingEditPartManager
				.getInstance().getEditpart(context, yDecimal);
		IWidgetPresentation<Component> presentation = decimalEditpart
				.getPresentation();
		DecimalField decimal = (DecimalField) presentation.getWidget();
	
		yDecimal.setValue(10);
		assertEquals(10, yDecimal.getValue(), 0);
		assertEquals("10,00 cm", decimal.getValue());
	
		decimal.setValue("10,00 cm");
		assertEquals(10, yDecimal.getValue(), 0);
		assertEquals("10,00 cm", decimal.getValue());
	}

	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_NumericToUomoConverter() throws Exception {
		// END SUPRESS CATCH EXCEPTION
		// build the view model
		// ...> yView
		// ......> yGridLayout
		// .........> yText
		YView yView = factory.createView();
		YGridLayout yGridlayout = factory.createGridLayout();
		yView.setContent(yGridlayout);
		YNumericField yNumber = factory.createNumericField();
		YNumericToUomoConverter yConverter = YConverterFactory.eINSTANCE.createYNumericToUomoConverter();
		yNumber.setConverter(yConverter);
		yGridlayout.getElements().add(yNumber);
	
		DelegatingConverterFactory.getInstance().addDelegate(
				new ConverterFactory(NUMBER_TO_UOMO_CONVERTER));
	
		VaadinRenderer renderer = new VaadinRenderer();
		IViewContext context = renderer.render(rootLayout, yView, null);
	
		INumericFieldEditpart numericEditpart = DelegatingEditPartManager
				.getInstance().getEditpart(context, yNumber);
		IWidgetPresentation<Component> presentation = numericEditpart
				.getPresentation();
		NumericField number = (NumericField) presentation.getWidget();
	
		yNumber.setValue(10);
		assertEquals(10, yNumber.getValue(), 0);
		assertEquals("10 cm", number.getValue());
	
		number.setValue("10 cm");
		assertEquals(10, yNumber.getValue(), 0);
		assertEquals("10 cm", number.getValue());
	}

	private static class ConverterFactory implements IConverterFactory {

		private String converterName;
		
		
		public ConverterFactory(String converterName) {
			super();
			this.converterName = converterName;
		}

		public ConverterFactory() {
			super();
		}
		
		@Override
		public boolean isFor(IViewContext uiContext, IConverterEditpart editpart) {
			return true;
		}

		@Override
		public Object createConverter(IViewContext uiContext,
				IConverterEditpart editpart) throws IllegalArgumentException {
			if (PresentationTests.NUMBER_TO_UOMO_CONVERTER.equals(converterName)){
				return new NumericToUomoConverter(uiContext, (YNumericToUomoConverter) editpart.getModel());
			} else if (PresentationTests.DECIMAL_TO_UOMO_CONVERTER.equals(converterName)){
					return new DecimalToUomoConverter(uiContext, (YDecimalToUomoConverter) editpart.getModel());
			}
			return new TextToTextConverter();
		}
	}

	/**
	 * Converters "Test" to "Test $" and "Test $" to "Test"
	 */
	@SuppressWarnings("serial")
	private static class TextToTextConverter implements
			Converter<String, String> {

		@Override
		public String convertToModel(String value,
				Class<? extends String> targetType, Locale locale)
				throws com.vaadin.data.util.converter.Converter.ConversionException {
			return value == null ? "" : value.replaceAll(" XX", "");
		}

		@Override
		public String convertToPresentation(String value,
				Class<? extends String> targetType, Locale locale)
				throws com.vaadin.data.util.converter.Converter.ConversionException {
			return value == null ? " XX" : value + " XX";
		}

		@Override
		public Class<String> getModelType() {
			return String.class;
		}

		@Override
		public Class<String> getPresentationType() {
			return String.class;
		}

	}
}
