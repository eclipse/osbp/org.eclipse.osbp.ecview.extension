/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.presentation.tests;

import static org.knowhowlab.osgi.testing.assertions.BundleAssert.assertBundleAvailable;
import static org.knowhowlab.osgi.testing.assertions.ServiceAssert.assertServiceAvailable;

import org.eclipse.osbp.runtime.common.event.IEventBroker;
import org.knowhowlab.osgi.testing.utils.BundleUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;

/**
 * Helps checking required bundles
 * 
 * @author admin
 * 
 */
public class BundleHelper {

	public static void ensureSetup() throws BundleException {
		ensureNeededBundlesAvailable();
		ensureNeededServicesAvailable();
	}

	public static void ensureNeededBundlesAvailable() throws BundleException {

		// check bundles available
		assertBundleAvailable("org.eclipse.equinox.ds");
		assertBundleAvailable("org.eclipse.equinox.event");
		assertBundleAvailable("org.eclipse.equinox.util");
		assertBundleAvailable("org.eclipse.osbp.runtime.event");
		assertBundleAvailable("org.eclipse.osbp.xtext.functionlibrarydsl.provider");

		// assertBundleAvailable("org.eclipse.equinox.cm");

		// start ds
		Bundle ds = BundleUtils.findBundle(Activator.context,
				"org.eclipse.equinox.ds");
		if (ds == null) {
			throw new IllegalStateException(
					"Bundle org.eclipse.equinox.ds is missing!");
		}
		if (ds.getState() != Bundle.STARTING && ds.getState() != Bundle.ACTIVE) {
			ds.start();
		}

		// start eqEvent
		Bundle eqEvent = BundleUtils.findBundle(Activator.context,
				"org.eclipse.equinox.event");
		if (eqEvent == null) {
			throw new IllegalStateException(
					"Bundle org.eclipse.equinox.event is missing!");
		}
		if (eqEvent.getState() != Bundle.STARTING
				&& eqEvent.getState() != Bundle.ACTIVE) {
			eqEvent.start();
		}

		// start event
		Bundle event = BundleUtils.findBundle(Activator.context,
				"org.eclipse.osbp.runtime.event");
		if (event == null) {
			throw new IllegalStateException(
					"Bundle org.eclipse.osbp.runtime.event is missing!");
		}
		if (event.getState() != Bundle.STARTING
				&& event.getState() != Bundle.ACTIVE) {
			event.start();
		}

		// start event
		Bundle function = BundleUtils.findBundle(Activator.context,
				"org.eclipse.osbp.xtext.functionlibrarydsl.provider");
		if (function == null) {
			throw new IllegalStateException(
					"Bundle org.eclipse.osbp.xtext.functionlibrarydsl.provider is missing!");
		}
		if (function.getState() != Bundle.STARTING
				&& function.getState() != Bundle.ACTIVE) {
			function.start();
		}
	}

	public static void ensureNeededServicesAvailable() throws BundleException {
		assertServiceAvailable(IEventBroker.class, 1000);
	}

}
