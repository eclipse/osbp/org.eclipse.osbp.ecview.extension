/**
 *                                                                            
 *  Copyright (c) 2015 - 2016 - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Loetz GmbH&Co.KG - Initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.presentation.tests.strategy;

import java.util.Date;

public class ValueBean extends AbstractBean {

	private String value;
	private byte[] bytesValue;
	private boolean boolValue;
	private Date dateValue;
	private int intValue;
	private long longValue;
	private double doubleValue;
	private float floatValue;

	public ValueBean(String value) {
		super();
		this.value = value;
	}
	
	public ValueBean(byte[] bytesValue) {
		super();
		this.bytesValue = bytesValue;
	}

	public ValueBean(boolean value) {
		super();
		this.boolValue = value;
	}

	public ValueBean(Date value) {
		super();
		this.dateValue = value;
	}

	public ValueBean(int value) {
		super();
		this.intValue = value;
	}

	public ValueBean(long value) {
		super();
		this.longValue = value;
	}

	public ValueBean(double value) {
		super();
		this.doubleValue = value;
	}

	public ValueBean(float value) {
		super();
		this.floatValue = value;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		firePropertyChanged("value", this.value, this.value = value);
	}

	public boolean isBoolValue() {
		return boolValue;
	}

	public void setBoolValue(boolean boolValue) {
		firePropertyChanged("boolValue", this.boolValue,
				this.boolValue = boolValue);
	}

	public Date getDateValue() {
		return dateValue;
	}

	public void setDateValue(Date dateValue) {
		firePropertyChanged("dateValue", this.dateValue,
				this.dateValue = dateValue);
	}

	public int getIntValue() {
		return intValue;
	}

	public void setIntValue(int intValue) {
		firePropertyChanged("intValue", this.intValue, this.intValue = intValue);
	}

	public long getLongValue() {
		return longValue;
	}

	public void setLongValue(long longValue) {
		firePropertyChanged("longValue", this.longValue,
				this.longValue = longValue);
	}

	public double getDoubleValue() {
		return doubleValue;
	}

	public void setDoubleValue(double doubleValue) {
		firePropertyChanged("doubleValue", this.doubleValue,
				this.doubleValue = doubleValue);
	}

	public float getFloatValue() {
		return floatValue;
	}

	public void setFloatValue(float floatValue) {
		firePropertyChanged("floatValue", this.floatValue,
				this.floatValue = floatValue);
	}

	public byte[] getBytesValue() {
		return bytesValue;
	}

	public void setBytesValue(byte[] bytesValue) {
		firePropertyChanged("bytesValue", this.bytesValue,
				this.bytesValue = bytesValue);
	}

}
