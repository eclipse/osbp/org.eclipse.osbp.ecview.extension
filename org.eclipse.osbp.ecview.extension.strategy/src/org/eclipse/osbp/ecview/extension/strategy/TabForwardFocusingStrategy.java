/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.strategy;

import org.eclipse.osbp.runtime.common.keystroke.KeyStrokeDefinition;

import com.vaadin.event.ShortcutAction.KeyCode;

/**
 * The Class TabForwardFocusingStrategy.
 */
public class TabForwardFocusingStrategy extends AbstractFocusingStrategy {
	public int getDirection() {
		return 1;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.extension.api.IFocusingStrategy#getKeyStrokeDefinition()
	 */
	@Override
	public KeyStrokeDefinition getKeyStrokeDefinition() {
		KeyStrokeDefinition def = new KeyStrokeDefinition("", KeyCode.TAB, new int[0]);
		return def;
	}

}