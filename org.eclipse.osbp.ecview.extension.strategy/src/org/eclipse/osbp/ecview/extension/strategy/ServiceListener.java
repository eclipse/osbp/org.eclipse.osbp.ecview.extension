/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.ecview.extension.strategy;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.osbp.ui.api.useraccess.IOrganizationService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The listener interface for receiving service events. The class that is
 * interested in processing a service event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addServiceListener</code> method. When the service event
 * occurs, that object's appropriate method is invoked.
 *
 */
@Component
public class ServiceListener {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceListener.class);

	/** The Constant sOrganizationServices. */
	private static final Map<String, IOrganizationService> sOrganizationServices = new HashMap<>();

	/**
	 * Gets the organization services.
	 *
	 * @return the organization services
	 */
	public static Map<String, IOrganizationService>  getOrganizationServices() {
		return sOrganizationServices;
	}

	/**
	 * Bind organization service.
	 *
	 * @param organization
	 *            the organization
	 */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	public void bindOrganizationService(IOrganizationService organization) {
		LOGGER.debug(ServiceListener.class.getCanonicalName() + ": " + organization.getClass().getCanonicalName() + " bound");
		sOrganizationServices.put(organization.getClass().getCanonicalName(), organization);
	}

	/**
	 * Unbind organization service.
	 *
	 * @param organization
	 *            the organization
	 */
	public void unbindOrganizationService(IOrganizationService organization) {
		LOGGER.debug(ServiceListener.class.getCanonicalName() + ": " + organization.getClass().getCanonicalName() + " unbound");
		sOrganizationServices.remove(organization.getClass().getCanonicalName());
	}
}
