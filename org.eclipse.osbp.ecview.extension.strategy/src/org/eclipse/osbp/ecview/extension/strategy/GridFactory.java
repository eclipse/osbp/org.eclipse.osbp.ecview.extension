/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.strategy;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.ecview.core.common.model.core.YConverter;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.runtime.common.annotations.DtoUtils;
import org.eclipse.osbp.runtime.common.layouting.IPropertyConstants;
import org.eclipse.osbp.utils.functionnormalizer.entities.FunctionType;
import org.eclipse.osbp.utils.functionnormalizer.api.FunctionTypingAPI;
import org.eclipse.osbp.ecview.extension.api.ILayoutingStrategy;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridColumn;
import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;
import org.eclipse.osbp.ecview.extension.model.YCollectionSuspect;
import org.eclipse.osbp.ecview.extension.model.YColumnInfo;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterFactory;

/**
 * A factory for creating Grid objects.
 */
public class GridFactory {

	/** The grid factory. */
	private CxGridFactory gridFactory = CxGridFactory.eINSTANCE;

	/**
	 * Creates a new Grid object.
	 *
	 * @param suspect
	 *            the suspect
	 * @return the cx grid
	 */
	public CxGrid createGrid(YCollectionSuspect suspect) {

		CxGrid grid = gridFactory.createCxGrid();
		grid.setId(suspect.getId());
		grid.setEditorEnabled(true);
		grid.setHeaderVisible(true);
		grid.setFilteringVisible(true);
		grid.setCustomFilters(false);
		grid.setUseBeanService(false);
		grid.setColumnReorderingAllowed(true);
		grid.setType(suspect.getType());
		grid.setTypeQualifiedName(suspect.getTypeQualifiedName());

		// lets iterate all the columns for the given suspect
		// and transform them to grid columns
		for (YColumnInfo info : suspect.getColumns()) {
			CxGridColumn col = gridFactory.createCxGridColumn();
			col.setName(info.getName());
			col.setLabelI18nKey(info.getLabelI18nKey());
			// We need to handle DTOs
			// for them we create a nested property eg. "productgroup.name"
			//
			Class<?> type = info.getType();
			if(type == null) {
				continue;
			}
			if (IDto.class.isAssignableFrom(type)) {
				Field domainKeyField = DtoUtils.getDomainKeyField(type);
				if (domainKeyField == null) {
					// we have no idea which field could be shown as a business
					// key
					continue;
				}

				// lets create a nested property path. Eg. "productgroup.name"
				String propertyPath = info.getName() + "."
						+ domainKeyField.getName();
				col.setPropertyPath(propertyPath);
				col.setPropertyId(propertyPath);
				col.setType(domainKeyField.getType());
				col.setTypeQualifiedName(domainKeyField.getType().getName());
			} else {
				col.setPropertyPath(info.getName());
				col.setPropertyId(info.getName());
				col.setType(info.getType());
				col.setTypeQualifiedName(info.getTypeQualifiedName());
			}

			col.setSourceType(info.getSourceType());
			col.setHideable(true);

			Pair pair = createRendererPair(info);
			col.setRenderer(pair.renderer);
			col.setConverter(pair.converter);

			// create the editor fields
			if (info.getType() == Boolean.class
					|| info.getType() == Boolean.TYPE) {
				col.setEditorField(ExtensionModelFactory.eINSTANCE
						.createYCheckBox());
				col.setEditable(true);
			} else if (info.getType() == Date.class) {
				col.setEditorField(ExtensionModelFactory.eINSTANCE
						.createYDateTime());
				col.setEditable(true);
				// } else if (info.getType() == Float.class
				// || info.getType() == Boolean.TYPE) {
				// YSlider pgbar =
				// ExtensionModelFactory.eINSTANCE.createYSlider();
				// pgbar.setMaxValue(1000d);
				// col.setEditorField(pgbar);
				// col.setEditable(true);
			} else {
				col.setEditorField(ExtensionModelFactory.eINSTANCE
						.createYTextField());
				col.setEditable(true);
			}

			grid.getColumns().add(col);
		}

		return grid;
	}

	/**
	 * Creates a renderer for the given property.
	 *
	 * @param info
	 *            the info
	 * @return the pair
	 */
	protected Pair createRendererPair(YColumnInfo info) {
		Pair pair = new Pair();
		pair.converter = AbstractLayoutingStrategy.getConverter(info.getProperties());
// ############################################
// TODO (JCD): The following lines provokes an java.lang.IllegalArgumentException 
// due to the fix use of a CustomDecimalConverter even not having java.lang.Double as property type 
// in the corresponding grid column
// ############################################
//		FunctionTypingAPI functionTypingApi = new FunctionTypingAPI();
//		for (FunctionType functionType : functionTypingApi.getTypes()) {
//			String functionPropertyValue = info.getProperties().get(
//					functionType.getName());
//			if (functionPropertyValue != null) {
//				pair.converter = YConverterFactory.eINSTANCE
//						.createYCustomDecimalConverter();
//				pair.converter.getProperties().addAll(info.getProperties());
//			}
//		}

		if (info.getType() == Boolean.class || info.getType() == Boolean.TYPE) {
			pair.renderer = CxGridRendererFactory.eINSTANCE
					.createCxGridBooleanRenderer();
		} else if (info.getType() == Date.class) {
			CxGridDateRenderer renderer = CxGridRendererFactory.eINSTANCE
					.createCxGridDateRenderer();
			// TODO set a dateformat here
			// renderer.setDateFormat(value);
			pair.renderer = renderer;
		// Blob check to display a blob image
		} else if ((info.getType() == String.class)
					&& ((info.getProperties() != null) || (!info.getProperties().isEmpty()))) {
				Stream<Entry<String, String>> propertyStream = info.getProperties().stream()
						.filter(prop -> prop.getKey().equalsIgnoreCase(IPropertyConstants.PROPERTY_BLOB));
				// Only if one 'Blob' or 'blob' property entry exists
				if (propertyStream.count() == 1) {
					pair.renderer = CxGridRendererFactory.eINSTANCE.createCxGridBlobImageRenderer();
					pair.converter = YConverterFactory.eINSTANCE.createYStringToByteArrayConverter();
				}

		} else {
			pair.renderer = CxGridRendererFactory.eINSTANCE
					.createCxGridTextRenderer();
		}

		return pair;
	}

	/**
	 * Creates a new Grid object.
	 *
	 * @return the pair
	 */
	protected Pair createBadRenderer() {
		Pair pair = new Pair();
		pair.renderer = CxGridRendererFactory.eINSTANCE
				.createCxGridTextRenderer();
		return pair;
	}

	/**
	 * The Class Pair.
	 */
	protected static class Pair {
		
		/** The renderer. */
		public CxGridRenderer renderer;
		
		/** The converter. */
		public YConverter converter;
	}

}
