/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.ecview.extension.strategy.util;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.TreeSet;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.printing.PDFPageable;
import org.eclipse.osbp.ui.api.user.IUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handle and use print services available on the web server
 */
public class PdfPrintService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PdfPrintService.class);
	private static PrintService[] printServices;
	
	/**
	 * @return the default print service name after re-looking up available print services
	 */
	public static String reLookupDefaultPrintServiceName() {
		return reLookupDefaultPrintServiceName(true);
	}
	
	/**
	 * @return the default print service name without re-looking up available print services
	 */
	public static String lookupDefaultPrintServiceName() {
		return reLookupDefaultPrintServiceName(false);
	}
	
	private static String reLookupDefaultPrintServiceName(boolean reLookup) {
		PrintService[] pss = reLookupPrintServices(reLookup);
		if	(pss.length > 0) {
			return pss[0].getName();
		}
		return null;
	}
	
	/**
	 * @return the sorted list of available print service names after re-looking up available print services
	 */
	public static Set<String> reLookupPrintServiceNames() {
		return reLookupDefaultPrintServiceNames(true);
	}
	
	/**
	 * @return the sorted list of available print service names without re-looking up available print services
	 */
	public static Set<String> lookupPrintServiceNames() {
		return reLookupDefaultPrintServiceNames(false);
	}
	
	private static Set<String> reLookupDefaultPrintServiceNames(boolean reLookup) {
		Set<String> psn = new TreeSet<>();
		PrintService[] pss = reLookupPrintServices(reLookup);
		if	(pss.length > 0) {
			for	(PrintService ps : pss) {
				psn.add(ps.getName());
			}
		}
		return psn;
	}
	
	/**
	 * @return the list of available print services after re-looking up available print services
	 */
	public static PrintService[] reLookupPrintServices() {
		return reLookupPrintServices(true);
	}
	
	/**
	 * @return the list of available print services without re-looking up available print services
	 */
	public static PrintService[] lookupPrintServices() {
		return reLookupPrintServices(false);
	}
	
	private static PrintService[] reLookupPrintServices(boolean reLookup) {
		if	(reLookup || (printServices == null)) {
			DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PAGEABLE;
			PrintRequestAttributeSet patts = new HashPrintRequestAttributeSet();
			//patts.add(Sides.DUPLEX);
			printServices = PrintServiceLookup.lookupPrintServices(flavor, patts);
		}
		return printServices;
	}

	/**
	 * @param name the name of the requested print service
	 * @return the print service instance without re-looking up
	 */
	public static PrintService getPrintService(String name) throws NullPointerException{
		PrintService[] pss = lookupPrintServices();
		if	(pss.length > 0) {
			for	(PrintService ps : pss) {
				if	(name.equals(ps.getName())) {
					return ps;
				}
			}
		}
		return pss[0];
	}
	
	/**
	 * @param pdfInputStream the input stream containing the pdf
	 * @param printServiceName the name of the printer service to be used to print, or <code>PdfPrintService.DEFAULT_PRINTER</code>
	 */
	public static void printReportAsPdf(InputStream pdfInputStream, String printServiceName) {
		printObjectAsPdf(pdfInputStream, printServiceName);
	}
	
	/**
	 * @param pdfInputStream the input stream containing the pdf
	 * @param printServiceName the name of the printer service to be used to print, or <code>PdfPrintService.DEFAULT_PRINTER</code>
	 */
	public static void printReportAsPdf(File pdfFile, String printServiceName) {
		printObjectAsPdf(pdfFile, printServiceName);
	}
	
	/**
	 * @param pdfInputStream the input stream containing the pdf
	 * @param printServiceName the name of the printer service to be used to print, or <code>PdfPrintService.DEFAULT_PRINTER</code>
	 */
	private static void printObjectAsPdf(Object object, String printServiceName) throws NotExistingPrintServiceNameException {
		try {
			PrintService printService = getPrintService(printServiceName);
			if	(printService != null) {
				PDDocument document = getPDDocument(object);
				if (document != null) {
					PrinterJob job = PrinterJob.getPrinterJob();
					job.setPageable(new PDFPageable(document));
					job.setPrintService(printService);
					job.print();
				}
			}
		} catch (NullPointerException e) {
			NotExistingPrintServiceNameException nepse = new NotExistingPrintServiceNameException();
			LOGGER.error(nepse.getMessage() + printServiceName, new NotExistingPrintServiceNameException());
		} catch (Exception e) {
			LOGGER.error("error while printing pdf file to " + printServiceName, e);
		}
	}
	
	private static PDDocument getPDDocument(Object object) throws InvalidPasswordException, IOException {
		if (object instanceof File){
			return PDDocument.load((File)object);
		} else if (object instanceof InputStream){
			return PDDocument.load((InputStream)object);
		}
		return null;
	}
}
