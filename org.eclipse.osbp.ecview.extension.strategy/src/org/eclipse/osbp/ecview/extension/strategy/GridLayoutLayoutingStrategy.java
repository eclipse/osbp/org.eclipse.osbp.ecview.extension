/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.strategy;

import java.util.ArrayList;

import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YFocusable;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayoutCellStyle;

import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ecview.extension.model.YSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;

/**
 * Vertical layouting.
 */
public class GridLayoutLayoutingStrategy extends AbstractLayoutingStrategy {

	/**
	 * The Enum Group.
	 */
	public enum Group {
		
		/** The discount. */
		DISCOUNT("discount"), 
		
		/** The nondiscount. */
		NONDISCOUNT("non-discount"), 
		
		/** The nothing. */
		NOTHING("nothing");

		/** The group id. */
		private final String groupId;

		/**
		 * Instantiates a new group.
		 *
		 * @param groupId
		 *            the group id
		 */
		Group(String groupId) {
			this.groupId = groupId;
		}
	}

	/**
	 * Instantiates a new grid layout layouting strategy.
	 */
	public GridLayoutLayoutingStrategy() {
		super(null);
	}

//	@Override
//	public void layout(YLayoutingInfo layoutingInfo) {
//		YStrategyLayout yLayout = layoutingInfo.getLayout();
//		layoutingInfo.getActiveSuspectInfos().clear();
//
//		YGridLayout content = (YGridLayout) createContentLayout();
//
//		content.setColumns(2);
//		int rowId1 = 0, rowId2 = 0;
//		YSuspectInfo lastInfo = null;
//		for (YSuspect suspect : new ArrayList<YSuspect>(
//				yLayout.getSuspects())) {
//			YSuspectInfo currentInfo = layoutingInfo
//					.createSuspectInfo(suspect);
//
//			// add the suspectInfo as active suspect
//			layoutingInfo.getActiveSuspectInfos().add(currentInfo);
//
//			YEmbeddable yEmbeddable = prepareElementForSuspect(currentInfo);
//
//			// A ->next B ->next C
//			if (lastInfo != null) {
//				lastInfo.setNextFocus(currentInfo);
//			} else if (yEmbeddable instanceof YFocusable) {
//				layoutingInfo.setFirstFocus(currentInfo);
//			}
//			lastInfo = currentInfo;
//
//			YSuspect ySuspect = currentInfo.getSuspect();
//			YGridLayoutCellStyle cellStyle = content
//					.addGridLayoutCellStyle(yEmbeddable);
//
//			// if (ySuspect.getTags().contains(Group.DISCOUNT.groupId)){
//			// cellStyle.addSpanInfo(0,rowId1,0,rowId1);
//			// rowId1++;
//			// } else {
//			// cellStyle.addSpanInfo(1,rowId2,1,rowId2);
//			// rowId2++;
//			// }
//			content.addElement(yEmbeddable);
//		}
//
//		layoutingInfo.setContent(content);
//	}

	/* (non-Javadoc)
 * @see org.eclipse.osbp.ecview.extension.strategy.AbstractLayoutingStrategy#createContentLayout()
 */
@Override
	protected YLayout createContentLayout() {
		return ExtensionModelFactory.eINSTANCE.createYGridLayout();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.strategy.AbstractLayoutingStrategy#addElement(org.eclipse.osbp.ecview.core.common.model.core.YLayout, org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable)
	 */
	@Override
	protected void addElement(YLayout layout, YEmbeddable element) {
		layout.addElement(element);
	}

}
