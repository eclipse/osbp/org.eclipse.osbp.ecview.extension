/**
 *                                                                            
 *  Copyright (c) 2011, 2019 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.strategy;

import org.eclipse.osbp.ecview.extension.api.ILayoutingStrategy;
import org.eclipse.osbp.ecview.extension.api.ILayoutingStrategyProvider;
import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = ILayoutingStrategyProvider.class, property = "ecview.layouting.id=Default")
public class DefaultLayoutingStrategyProvider implements ILayoutingStrategyProvider {
	private static final String ID = "DefaultLayoutingStrategy";

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.extension.api.ILayoutingStrategyProvider#
	 * getStrategy()
	 */
	@Override
	public ILayoutingStrategy getStrategy() {
		return new FormLayoutLayoutingStrategy();
	}
}