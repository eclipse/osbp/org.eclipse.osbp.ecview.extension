/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.presentation.renderer;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.util.converter.Converter.ConversionException;
import com.vaadin.ui.Grid.AbstractRenderer;

import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer;

import elemental.json.JsonValue;

/**
 * The Class QuantityRenderer.
 */
@SuppressWarnings("serial")
public class QuantityRenderer extends AbstractRenderer<Object> {

	/** The Constant HTML_PATTERN. */
	private static final String HTML_PATTERN = "<b>{@value}</b> <i>{@uom}</i>";

	/** The Constant NUMBER_FORMAT. */
	private static final String NUMBER_FORMAT = "#,##0.00";

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(QuantityRenderer.class);

	/** The locale. */
	private final Locale locale;

	/** The cx renderer. */
	private CxGridQuantityRenderer cxRenderer;

	/**
	 * Instantiates a new quantity renderer.
	 *
	 * @param cxRenderer
	 *            the cx renderer
	 * @param locale
	 *            the locale
	 */
	public QuantityRenderer(CxGridQuantityRenderer cxRenderer, Locale locale) {
		super(Object.class, cxRenderer.getNullRepresentation());
		this.cxRenderer = cxRenderer;
		this.locale = locale;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.Grid.AbstractRenderer#encode(java.lang.Object)
	 */
	@Override
	public JsonValue encode(Object value) {
		String stringValue;
		if (value == null) {
			stringValue = getNullRepresentation();
		} else {
			String valueString = getValueString(value);
			Object uomString = getUomString(value);
			stringValue = toHtmlPattern().replaceAll("\\{@value}", valueString);
			stringValue = stringValue.replaceAll("\\{@uom}",
					uomString.toString());
		}
		return encode(stringValue, String.class);
	}

	/**
	 * Gets the uom string.
	 *
	 * @param value
	 *            the value
	 * @return the uom string
	 */
	protected Object getUomString(Object value) {
		Object uomProp;
		try {
			uomProp = PropertyUtils.getProperty(value,
					cxRenderer.getUomPropertyPath());
		} catch (IllegalAccessException | InvocationTargetException
				| NoSuchMethodException e) {
			uomProp = "";
			LOGGER.error("{}", e);
		} catch (NestedNullException e) {
			// nothing to do -> value is null
			uomProp = "";
		}
		return uomProp;
	}

	/**
	 * Gets the value string.
	 *
	 * @param value
	 *            the value
	 * @return the value string
	 */
	protected String getValueString(Object value) {
		String valueString = "undef";
		Object valueProp;
		try {
			valueProp = PropertyUtils.getProperty(value,
					cxRenderer.getValuePropertyPath());
			if (valueProp instanceof String) {
				valueString = (String) valueProp;
			} else {
				DecimalFormat df = new DecimalFormat(
						StringEscapeUtils.unescapeHtml(getNumberFormat()),
						DecimalFormatSymbols.getInstance(locale));
				valueString = df.format(valueProp);
			}
		} catch (IllegalArgumentException e) {
			String msg = String.format(
					"formatter %s is invalid for decimal numbers: %s",
					getNumberFormat(), e.getLocalizedMessage());
			throw new ConversionException(msg);
		} catch (IllegalAccessException | InvocationTargetException
				| NoSuchMethodException e) {
			LOGGER.error("{}", e);
		}
		return valueString;
	}

	/**
	 * To html pattern.
	 *
	 * @return the string
	 */
	protected String toHtmlPattern() {
		return cxRenderer.getHtmlPattern() == null
				|| cxRenderer.getHtmlPattern().equals("") ? HTML_PATTERN
				: cxRenderer.getHtmlPattern();
	}

	/**
	 * Gets the number format.
	 *
	 * @return the number format
	 */
	private String getNumberFormat() {
		return cxRenderer.getNumberFormat() != null
				&& !cxRenderer.getNumberFormat().trim().isEmpty() ? cxRenderer
				.getNumberFormat() : NUMBER_FORMAT;
	}
}
