/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.presentation.renderer;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.extension.grid.editparts.renderer.IGridRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.renderer.IGridRendererFactory;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBooleanRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridHtmlRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridTextRenderer;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.VaadinRenderer;
import org.osgi.service.component.annotations.Component;
import org.vaadin.grid.cellrenderers.view.BlobImageRenderer;
import org.vaadin.gridutil.renderer.BooleanRenderer;
import org.vaadin.gridutil.renderer.IndicatorRenderer;

import com.vaadin.data.util.converter.Converter.ConversionException;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.ImageRenderer;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.ProgressBarRenderer;
import com.vaadin.ui.renderers.TextRenderer;

import elemental.json.JsonValue;

/**
 * A factory for creating Renderer objects.
 */
@Component(immediate = true)
public class RendererFactory implements IGridRendererFactory {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.extension.grid.editparts.renderer.
	 * IGridRendererFactory
	 * #isFor(org.eclipse.osbp.ecview.core.common.context.IViewContext,
	 * org.eclipse
	 * .osbp.ecview.extension.grid.editparts.renderer.IGridRendererEditpart)
	 */
	@Override
	public boolean isFor(IViewContext uiContext, IGridRendererEditpart editpart) {
		String presentationURI = uiContext.getPresentationURI();
		return presentationURI != null
				&& presentationURI.equals(VaadinRenderer.UI_KIT_URI);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.extension.grid.editparts.renderer.
	 * IGridRendererFactory
	 * #createRenderer(org.eclipse.osbp.ecview.core.common.context.IViewContext,
	 * org
	 * .eclipse.osbp.ecview.extension.grid.editparts.renderer.IGridRendererEditpart
	 * )
	 */
	@SuppressWarnings("serial")
	@Override
	public Object createRenderer(IViewContext uiContext,
			IGridRendererEditpart editpart) throws IllegalArgumentException {

		CxGridRenderer cxRenderer = (CxGridRenderer) editpart.getModel();
		if (cxRenderer instanceof CxGridDateRenderer) {
			CxGridDateRenderer cxCasted = (CxGridDateRenderer) cxRenderer;
			String dateFormat = cxCasted.getDateFormat();
			if (dateFormat != null && !dateFormat.equals("")) {
				return new DateRenderer(new SimpleDateFormat(dateFormat,
						uiContext.getLocale()));
			} else {
				return new DateRenderer(DateFormat.getDateInstance(DateFormat.DEFAULT, uiContext.getLocale()));
			}
		} else if (cxRenderer instanceof CxGridNumberRenderer) {
			CxGridNumberRenderer cxCasted = (CxGridNumberRenderer) cxRenderer;
			String numberFormat = cxCasted.getNumberFormat();
			if (numberFormat != null && !numberFormat.equals("")) {
				try {
					return new NumberRenderer(new DecimalFormat(
							StringEscapeUtils.unescapeHtml(numberFormat),
							DecimalFormatSymbols.getInstance(uiContext
									.getLocale())),
							cxCasted.getNullRepresentation());
				} catch (IllegalArgumentException e) {
					String msg = String.format(
							"formatter %s is invalid for decimal numbers: %s",
							numberFormat, e.getLocalizedMessage());
					throw new ConversionException(msg);
				}
			} else {
				return new NumberRenderer(uiContext.getLocale());
			}
		} else if (cxRenderer instanceof CxGridTextRenderer) {
			CxGridTextRenderer cxCasted = (CxGridTextRenderer) cxRenderer;
			return new TextRenderer(cxCasted.getNullRepresentation());
		} else if (cxRenderer instanceof CxGridImageRenderer) {
			CxGridImageRenderer cxCasted = (CxGridImageRenderer) cxRenderer;
			ImageRenderer renderer = new ImageRenderer();
			renderer.addClickListener(e -> {
				CxGridRendererClickEvent cxEvent = CxGridRendererFactory.eINSTANCE
						.createCxGridRendererClickEvent();
				cxEvent.setRenderer(cxRenderer);
				cxEvent.setLastClickTime(new Date().getTime());
				cxCasted.setLastClickEvent(cxEvent);
			});
			return renderer;
		} else if (cxRenderer instanceof CxGridBlobImageRenderer) {
//			CxGridImageRenderer cxCasted = (CxGridImageRenderer) cxRenderer;
			BlobImageRenderer renderer = new BlobImageRenderer();
//			renderer.addClickListener(e -> {
//				CxGridRendererClickEvent cxEvent = CxGridRendererFactory.eINSTANCE
//						.createCxGridRendererClickEvent();
//				cxEvent.setRenderer(cxRenderer);
//				cxEvent.setLastClickTime(new Date().getTime());
//				cxCasted.setLastClickEvent(cxEvent);
//			});
			return renderer;
		} else if (cxRenderer instanceof CxGridButtonRenderer) {
			CxGridButtonRenderer cxCasted = (CxGridButtonRenderer) cxRenderer;
			ButtonRenderer renderer = new ButtonRenderer(
					cxCasted.getNullRepresentation());
			renderer.addClickListener(e -> {
				CxGridRendererClickEvent cxEvent = CxGridRendererFactory.eINSTANCE
						.createCxGridRendererClickEvent();
				cxEvent.setRenderer(cxRenderer);
				cxEvent.setLastClickTime(new Date().getTime());
				cxCasted.setLastClickEvent(cxEvent);
			});
			return renderer;
		} else if (cxRenderer instanceof CxGridHtmlRenderer) {
			CxGridHtmlRenderer cxCasted = (CxGridHtmlRenderer) cxRenderer;
			return new HtmlRenderer(cxCasted.getNullRepresentation());
		} else if (cxRenderer instanceof CxGridProgressBarRenderer) {
			final CxGridProgressBarRenderer cxCasted = (CxGridProgressBarRenderer) cxRenderer;
			return new ProgressBarRenderer() {
				@Override
				public JsonValue encode(Double value) {
					if (value != null) {
						value = value / cxCasted.getMaxValue();
					}
					return super.encode(value);
				}
			};
		} else if (cxRenderer instanceof CxGridBooleanRenderer) {
			return new BooleanRenderer();
		} else if (cxRenderer instanceof CxGridQuantityRenderer) {
			return new QuantityRenderer((CxGridQuantityRenderer) cxRenderer,
					uiContext.getLocale());
		} else if (cxRenderer instanceof CxGridPriceRenderer) {
			return new PriceRenderer((CxGridPriceRenderer) cxRenderer,
					uiContext.getLocale());
		} else if (cxRenderer instanceof CxGridIndicatorRenderer) {
			CxGridIndicatorRenderer temp = (CxGridIndicatorRenderer) cxRenderer;
			return new IndicatorRenderer(temp.getGreenStarts(),
					temp.getRedEnds());
		}
		return null;
	}

}
