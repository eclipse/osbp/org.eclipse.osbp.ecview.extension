/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.presentation.converter;

import java.lang.reflect.InvocationTargetException;
import java.util.Locale;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.converter.IGridNestedPropertyConverterEditpart;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.util.converter.Converter;

public class GridNestedPropertyConverter implements Converter<Object, Object> {

	private static final Logger LOGGER = LoggerFactory.getLogger(GridNestedPropertyConverter.class);

	final IGridNestedPropertyConverterEditpart editpart;
	final CxGridNestedConverter model;

	Converter<Object, Object> nestedTypeConverter;

	public GridNestedPropertyConverter(IGridNestedPropertyConverterEditpart editpart) {
		this.editpart = editpart;
		this.model = (CxGridNestedConverter) editpart.getModel();

	}

	@Override
	public Object convertToModel(Object value, Class<? extends Object> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		throw new UnsupportedOperationException("Not a valid call!");
	}

	@Override
	public Object convertToPresentation(Object value, Class<? extends Object> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {

		checkDelegatingConverter();

		if (value != null && model.getNestedDotPath() != null && !model.getNestedDotPath().equals("")) {
			Object property = null;
			try {
				property = PropertyUtils.getProperty(value, model.getNestedDotPath());
				if (nestedTypeConverter != null) {
					property = nestedTypeConverter.convertToPresentation(property, model.getNestedType(), locale);
				}
				return property;
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException
					| NestedNullException e) {
				LOGGER.error("{}", e);
				throw new IllegalStateException();
			}
		} else {
			return value;
		}
	}

	@SuppressWarnings("unchecked")
	private void checkDelegatingConverter() {
		if (model.getNestedTypeConverter() != null && nestedTypeConverter == null) {
			IConverterEditpart nestedTypeConverterEP = DelegatingEditPartManager.getInstance()
					.getEditpart(editpart.getContext(), model.getNestedTypeConverter());
			nestedTypeConverter = (Converter<Object, Object>) nestedTypeConverterEP.getDelegate();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<Object> getModelType() {
		return (Class<Object>) model.getBaseType();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<Object> getPresentationType() {
		return (Class<Object>) model.getNestedType();
	}

}
