package org.eclipse.osbp.ecview.extension.tests.dtos;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Locale;
import org.eclipse.osbp.runtime.common.i18n.ITranslator;
import org.slf4j.Logger;

@SuppressWarnings("all")
public class TestsDtosTranslator implements ITranslator {
  private static Logger log = org.slf4j.LoggerFactory.getLogger("translations by translator");
  
  private static TestsDtosTranslator instance = null;
  
  private static Locale lastLocale = null;
  
  private PropertyChangeSupport pcs = new java.beans.PropertyChangeSupport(this);
  
  private HashMap<String, String> translations = new HashMap<String,String>() {{
    }};
  
  public static TestsDtosTranslator getInstance(final Locale locale) {
    if(instance == null) {
    	instance = new org.eclipse.osbp.ecview.extension.tests.dtos.TestsDtosTranslator();
    	if (lastLocale == null) {
    		instance.changeLocale(locale);
    	}
    }
    return instance;
  }
  
  public void addPropertyChangeListener(final PropertyChangeListener listener) {
    this.pcs.addPropertyChangeListener(listener);
  }
  
  public void removePropertyChangeListener(final PropertyChangeListener listener) {
    this.pcs.removePropertyChangeListener(listener);
  }
  
  public void mergeTranslations() {
    
  }
  
  public HashMap<String, String> getTranslations() {
    return translations;
  }
  
  public void changeLocale(final Locale locale) {
    // avoid unnecessary settings
    if (locale == null) {
    	if (log.isDebugEnabled()) log.debug("locale for org.eclipse.osbp.ecview.extension.tests.dtos is null.");
    	return;
    }
    if (locale.equals(lastLocale)) {
    	if (log.isDebugEnabled()) log.debug("locale for org.eclipse.osbp.ecview.extension.tests.dtos already set to "+locale.getDisplayLanguage());
    	return;
    }
    if (log.isDebugEnabled()) log.debug("locale for org.eclipse.osbp.ecview.extension.tests.dtos set to "+locale.getDisplayLanguage());
    lastLocale = locale;
    // call the imported translators change locale method
    try {
    	java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("i18n.I18N", locale, getClass().getClassLoader());
    } catch (java.util.MissingResourceException mre) {
    	System.err.println(getClass().getCanonicalName()+" - "+mre.getLocalizedMessage());
    }
    
  }
}
