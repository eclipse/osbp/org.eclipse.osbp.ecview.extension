<?xml version="1.0" encoding="UTF-8"?>
<!--#======================================================================= -->
<!--# Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany). -->
<!--# All rights reserved. This program and the accompanying materials -->
<!--# are made available under the terms of the Eclipse Public License 2.0  -->
<!--# which accompanies this distribution, and is available at -->
<!--# https://www.eclipse.org/legal/epl-2.0/    -->
<!--#                                           -->
<!--# SPDX-License-Identifier: EPL-2.0          -->
<!--# -->
<!--# Contributors: -->
<!--# Cristiano Gavi??o - initial API and implementation -->
<!--#======================================================================= -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.eclipse.osbp.ecview.extension</groupId>
		<artifactId>org.eclipse.osbp.ecview.extension.aggregator</artifactId>
		<version>0.9.0-SNAPSHOT</version>
		<relativePath>..</relativePath>
	</parent>
	<artifactId>org.eclipse.osbp.ecview.extension.grid.presentation.clientside</artifactId>
	<packaging>eclipse-plugin</packaging>
	<description>Clientside Vaadin presentation layer for OSBP ECView extension</description>
	
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
	</properties>

<!-- no dependencies after o.e.o.dependencies	
	<dependencies>
		<dependency>
			<groupId>org.eclipse.osbp.ecview.extension</groupId>
			<artifactId>org.eclipse.osbp.ecview.extension.grid.presentation</artifactId>
			<version>0.9.0-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>com.vaadin</groupId>
			<artifactId>vaadin-server</artifactId>
			<scope>provided</scope>
			<version>${vaadin.version}</version>
		</dependency>
		<dependency>
			<groupId>com.vaadin</groupId>
			<artifactId>vaadin-client-compiled</artifactId>
			<scope>provided</scope>
			<version>${vaadin.version}</version>
		</dependency>
		<dependency>
			<groupId>com.vaadin</groupId>
			<artifactId>vaadin-shared</artifactId>
			<scope>provided</scope>
			<version>${vaadin.version}</version>
		</dependency>
		<dependency>
			<groupId>com.vaadin</groupId>
			<artifactId>vaadin-themes</artifactId>
			<version>${vaadin.version}</version>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.4</version>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>servlet-api</artifactId>
			<version>2.5</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>commons-beanutils</groupId>
			<artifactId>commons-beanutils</artifactId>
			<version>1.8.3</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>1.7.2</version>
		</dependency>
	</dependencies>
-->			
	<!-- dependencies needed for the widgetset compiler -->
	<dependencies>
		<dependency>
			<groupId>com.vaadin</groupId>
			<artifactId>vaadin-client</artifactId>
			<version>${vaadin.version}</version>
		</dependency>
		<dependency>
			<groupId>com.vaadin</groupId>
			<artifactId>vaadin-client-compiler</artifactId>
			<version>${vaadin.version}</version>
		</dependency>
		<dependency>
			<groupId>org.eclipse.osbp.ecview.extension</groupId>
			<artifactId>
				org.eclipse.osbp.ecview.extension.grid.presentation
			</artifactId>
			<version>0.9.0-SNAPSHOT</version>
			<type>eclipse-plugin</type>
		</dependency>
	</dependencies>
	
	
	<build>
		<resources>
			<resource>
				<!-- This add sources, .gwt.xml files etc to jar from source directory. 
					Not quite maven convention byt easiest to setup this way. -->
				<directory>src</directory>
			</resource>
			<resource>
				<directory>src</directory>
			</resource>
		</resources>
		<sourceDirectory>src</sourceDirectory>

		<plugins>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-maven-plugin</artifactId>
				<extensions>true</extensions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<!-- <includes> -->
					<!-- <include>org.eclipse.osbp/runtime/web/vaadin/components/widgets/*.java</include> -->
					<!-- </includes> -->
				</configuration>
			</plugin>
			<plugin>
				<groupId>com.vaadin</groupId>
				<artifactId>vaadin-maven-plugin</artifactId>
				<version>${vaadin.plugin.version}</version>
				<configuration>
					<extraJvmArgs>-Xmx1024M -Xss1024k</extraJvmArgs>
					<!-- <runTarget>mobilemail</runTarget> -->
					<!-- We are doing "inplace" but into subdir VAADIN/widgetsets. This 
						way compatible with Vaadin eclipse plugin. -->
					<webappDirectory>${basedir}/VAADIN/widgetsets
					</webappDirectory>
					<hostedWebapp>${basedir}/VAADIN/widgetsets
					</hostedWebapp>
					<noServer>true</noServer>
					<!-- Remove draftCompile when project is ready -->
					<draftCompile>true</draftCompile>
					<strict>true</strict>
					<compileReport>false</compileReport>
					<style>PRETTY</style>
					<logLevel>INFO</logLevel>
					<runTarget>http://localhost:8080/</runTarget>
					<failOnError>false</failOnError>
					<!-- Remove these when going to release dependencies -->
					<force>true</force>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>resources</goal>
							<goal>compile</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<!-- As we are doing "inplace" GWT compilation, ensure the widgetset -->
			<!-- directory is cleaned properly -->
			<plugin>
				<artifactId>maven-clean-plugin</artifactId>
				<configuration>
					<filesets>
						<fileset>
							<directory>${basedir}/VAADIN/widgetsets</directory>
						</fileset>
					</filesets>
				</configuration>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>com.vaadin</groupId>
										<artifactId>vaadin-maven-plugin</artifactId>
										<versionRange>[2.3.0-1,)</versionRange>
										<goals>
											<goal>resources</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore/>
									</action>
								</pluginExecution>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>com.vaadin</groupId>
										<artifactId>vaadin-maven-plugin</artifactId>
										<versionRange>[1.0.2,)</versionRange>
										<goals>
											<goal>update-widgetset</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore/>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
	
</project>
