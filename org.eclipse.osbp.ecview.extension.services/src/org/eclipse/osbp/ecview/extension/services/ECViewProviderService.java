/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.services;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.context.ViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.extender.IECViewCache;
import org.eclipse.osbp.ecview.core.common.extender.IECViewProviderService;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YVisibilityProcessorValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlot;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YAuthorizationVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

/**
 * The Class ECViewProviderService.
 */
@Component
public class ECViewProviderService implements IECViewProviderService {

	/** The cache. */
	private IECViewCache cache;
	/**
	 * Use this property to pass the beanSlot that should be used for the dto.
	 * See properties map in {@link #render(Object, Map)}
	 */
	public static final String PROP_SLOT = "beanSlot";

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.extender.IECViewProviderService#getViewContext(java.lang.Class, java.lang.String)
	 */
	@Override
	public IViewContext getViewContext(Class<?> dtoClass, String beanSlot) {
		YView yView = createYView(dtoClass, beanSlot);
		return createViewContext(yView);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.extender.IECViewProviderService#getViewContext(java.lang.String)
	 */
	@Override
	public IViewContext getViewContext(String viewId) {
		YView yView = cache.getView(viewId);
		return createViewContext(yView);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.extender.IECViewProviderService#getViewContext(org.eclipse.osbp.ecview.core.common.model.core.YView)
	 */
	@Override
	public IViewContext getViewContext(YView yView) {
		return createViewContext(yView);
	}

	/**
	 * Creates the view context.
	 *
	 * @param yView
	 *            the y view
	 * @return the view context
	 */
	protected ViewContext createViewContext(YView yView) {
		if (yView != null) {
			// create a unrendered view context
			ViewContext viewContext = new ViewContext();
			DelegatingEditPartManager.getInstance().getEditpart(viewContext,
					yView);
			return viewContext;
		} else {
			return null;
		}
	}

	/**
	 * This is the main entry point to serve different YViews for the given DTO.
	 *
	 * @param dtoClass
	 *            the dto class
	 * @param beanSlot
	 *            the bean slot
	 * @return the y view
	 */
	protected YView createYView(Class<?> dtoClass, String beanSlot) {
		// default way -> look in the cache for views
		List<YView> temp = cache.getViews(dtoClass, beanSlot);

		YView yView = temp.size() > 0 ? temp.get(0) : null;

		if (yView != null) {
			doEnhance(yView);
		}

		return yView;
	}

	/**
	 * Do enhance.
	 *
	 * @param yView
	 *            the y view
	 */
	protected void doEnhance(YView yView) {
		// createVisibilityProcessor(yView, properties);
	}

	/**
	 * This is the main entry point to serve different YViews for the given DTO.
	 *
	 * @param viewId
	 *            the view id
	 * @return the y view
	 */
	protected YView createYView(String viewId) {
		return cache.getView(viewId);
	}

	/**
	 * Bind cache.
	 *
	 * @param cache
	 *            the cache
	 */
	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC, unbind = "unbindCache")
	protected void bindCache(IECViewCache cache) {
		this.cache = cache;
	}

	/**
	 * Unbind cache.
	 *
	 * @param cache
	 *            the cache
	 */
	protected void unbindCache(IECViewCache cache) {
		this.cache = null;
	}

	/**
	 * Creates the visibility processor.
	 *
	 * @param yView
	 *            the y view
	 * @param properties
	 *            the properties
	 */
	private void createVisibilityProcessor(YView yView,
			Map<String, Object> properties) {
		// create visibilityProcessor
		YAuthorizationVisibilityProcessor yvp = YVisibilityFactory.eINSTANCE
				.createYAuthorizationVisibilityProcessor();

		// yvp.setDelegate(visibilityProcessor);
		// yvp.setDelegateQualifiedName(visibilityProcessor.getCanonicalName());

		// add visibilityProcessor to YView
		EList<YVisibilityProcessor> visibilityProcessors = yView
				.getVisibilityProcessors();
		visibilityProcessors.add(yvp);
		// create bean slot endpoint
		YBeanSlot beanSlot = yView.getBeanSlot(getBeanSlotName(properties));
		YBeanSlotValueBindingEndpoint sourceEP = CoreModelFactory.eINSTANCE
				.createYBeanSlotValueBindingEndpoint();
		sourceEP.setBeanSlot(beanSlot);

		YVisibilityProcessorValueBindingEndpoint targetEP = BindingFactory.eINSTANCE
				.createYVisibilityProcessorValueBindingEndpoint();
		// targetEP.setProperty(dsDtoName);
		// create a binding from beanslot to visibilityProcessor
		YValueBinding binding = BindingFactory.eINSTANCE.createYValueBinding();
		binding.setTargetEndpoint(targetEP);
		binding.setModelEndpoint(sourceEP);
		binding.setModelToTargetStrategy(YBindingUpdateStrategy.UPDATE);
		binding.setTargetToModelStrategy(YBindingUpdateStrategy.NEVER);
		yvp.getTriggersOn().add(binding);
	}

	/**
	 * Gets the bean slot name.
	 *
	 * @param properties
	 *            the properties
	 * @return the bean slot name
	 */
	protected String getBeanSlotName(Map<String, Object> properties) {
		return (String) properties.get(PROP_SLOT);
	}

}
