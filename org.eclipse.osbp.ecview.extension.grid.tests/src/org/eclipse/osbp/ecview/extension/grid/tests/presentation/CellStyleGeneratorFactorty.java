/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.grid.tests.presentation;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.osgi.service.component.annotations.Component;

import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.CellStyleGenerator;

import org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridCellStyleGeneratorEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.presentation.IGridCellStyleGeneratorFactory;

@Component(immediate = true)
public class CellStyleGeneratorFactorty implements
		IGridCellStyleGeneratorFactory {

	@Override
	public boolean isFor(IViewContext uiContext,
			IGridCellStyleGeneratorEditpart editpart) {
		return true;
	}

	@Override
	public Object createCellStyleGenerator(IViewContext uiContext,
			IGridCellStyleGeneratorEditpart editpart)
			throws IllegalArgumentException {

		CxGridDelegateCellStyleGenerator yGenerator = (CxGridDelegateCellStyleGenerator) editpart
				.getModel();
		if (yGenerator.getDelegateId().equals("sampleGrid")) {
			return new Generator();
		}

		return null;

	}

	@SuppressWarnings("serial")
	private static class Generator implements CellStyleGenerator {

		@Override
		public String getStyle(CellReference cellReference) {
			if (cellReference.getPropertyId().equals("name")) {
				return "osbee-bold";
			}
			return null;
		}
	}

}
