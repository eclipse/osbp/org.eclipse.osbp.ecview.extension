/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.grid.tests.presentation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Locale;

import javax.naming.ConfigurationException;

import org.eclipse.osbp.ecview.core.common.context.ContextException;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YDelegateConverter;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDateTime;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSelectionType;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.util.SimpleExtensionModelFactory;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.IDateTimeEditpart;
import org.eclipse.osbp.ecview.core.util.emf.ModelUtil;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridColumn;
import org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;
import org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridColumnEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridFooterRowEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridHeaderRowEditpart;
import org.eclipse.osbp.ecview.extension.grid.presentation.GridPresentation;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;
import org.eclipse.osbp.runtime.common.dispose.AbstractDisposable;
import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.VaadinRenderer;
import org.eclipse.osbp.runtime.web.vaadin.components.fields.NumericField;
import org.eclipse.osbp.runtime.web.vaadin.components.fields.search.TextSearchField;
import org.eclipse.osbp.runtime.web.vaadin.databinding.VaadinObservables;
import org.junit.Before;
import org.junit.Test;
import org.knowhowlab.osgi.testing.utils.BundleUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.annotations.Component;

import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.data.util.converter.StringToIntegerConverter;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.grid.GridStaticCellType;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.FooterCell;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.ImageRenderer;
import com.vaadin.ui.renderers.ProgressBarRenderer;
import com.vaadin.ui.renderers.TextRenderer;

/**
 * Tests the {@link GridPresentation}.
 */
@SuppressWarnings("restriction")
public class GridTests {

	private static final String P_BIRTHDAY = "birthday";
	private static final String P_VITALITY = "vitality";
	private static final String P_GENDER = "gender";
	private static final String P_NATIVE_GERMAN = "nativeGerman";
	private static final String P_AGE = "age";
	private static final String P_NAME2 = "name2";
	private static final String P_NAME = "name";
	private SimpleExtensionModelFactory factory = new SimpleExtensionModelFactory();
	private CxGridFactory cxFactory = CxGridFactory.eINSTANCE;
	private CssLayout rootLayout = new CssLayout();
	private CxGridColumn cxColName;
	private CxGridColumn cxColName2;
	private CxGridColumn cxColAge;
	private CxGridButtonRenderer cxAgeRenderer;
	private YECViewModelValueBindingEndpoint cxAgeRendererClickEndpoint;
	private CxGridColumn cxColNativeGerman;
	private CxGridColumn cxColGender;
	private CxGridColumn cxColVit;
	private CxGridProgressBarRenderer cxVitRenderer;
	private CxGridColumn cxColBirthday;
	private CxGridDateRenderer cxBirthdayRenderer;
	private YView yView;
	private CxGrid cxGrid;
	private CxGridGroupedCell cxNamesGroupCell;
	private CxGridHeaderRow cxHeaderRow1;
	private CxGridMetaCell cxCustomCellAge;
	private CxGridGroupedCell cxPersonGroupCell;
	private CxGridHeaderRow cxHeaderRow2;
	private CxGridMetaCell cxCustomCellBirthday;
	private CxGridGroupedCell cxFooterGroup1;
	private CxGridFooterRow cxFooterRow;
	private CxGridGroupedCell cxFooterGroup2;
	private CxGridFooterRow cxFooterRowPersonInfo;
	private CxGridMetaCell cxCustomCellBirthdayFooter;

	@SuppressWarnings("unchecked")
	private static <A> A findEditpart(YElement yElement) {
		return (A) DelegatingEditPartManager.getInstance().findEditpart(
				yElement);
	}

	/**
	 * Setup tests.
	 * 
	 * @throws ConfigurationException
	 * @throws BundleException
	 */
	@Before
	public void setup() throws BundleException {
		BundleUtils.startBundleAsync(getBundleContext(),
				"org.eclipse.osbp.runtime.event");

		VaadinSession.setCurrent(new VaadinSession(null));
		UI ui = new DefaultUI();
		ui.setSession(VaadinSession.getCurrent());
		UI.setCurrent(ui);
		UI.getCurrent().setContent(rootLayout);
		VaadinObservables.activateRealm(UI.getCurrent());
	}

	private BundleContext getBundleContext() {
		return FrameworkUtil.getBundle(getClass()).getBundleContext();
	}

	/**
	 * A very simple render.
	 * 
	 * @throws ContextException
	 */
	@Test
	public void test_SimpleRender() throws ContextException {

		// switch the global locale to german
		Locale.setDefault(Locale.GERMAN);

		// ----------------------------------------------------------
		// ------------- Create the grid --------------------------
		// ----------------------------------------------------------
		YView yView = factory.createView();
		CxGrid cxGrid = cxFactory.createCxGrid();
		yView.setContent(cxGrid);

		// ----------------------------------------------------------
		// ----------------- Render the UI --------------------------
		// ----------------------------------------------------------
		VaadinRenderer renderer = new VaadinRenderer();
		IViewContext context = renderer.render(rootLayout, yView, null);
		IGridEditpart gridEP = DelegatingEditPartManager.getInstance()
				.getEditpart(context, cxGrid);
		GridPresentation gridPres = gridEP.getPresentation();

		assertTrue(gridPres.isRendered());
		assertFalse(gridPres.isDisposed());
		assertTrue(gridEP.isRendered());
		assertFalse(gridEP.isDisposed());

		gridEP.requestDispose();
		assertFalse(gridPres.isRendered());
		assertTrue(gridPres.isDisposed());
		assertFalse(gridEP.isRendered());
		assertTrue(gridEP.isDisposed());
	}

	/**
	 * A very simple header row.
	 * 
	 * @throws ContextException
	 */
	@Test
	public void test_Columns() throws ContextException {

		assertNotNull(VaadinSession.getCurrent());

		// switch the global locale to german
		Locale.setDefault(Locale.GERMAN);

		// ----------------------------------------------------------
		// ------------- Create the layout --------------------------
		// ----------------------------------------------------------
		createDefaultModel();

		// ----------------------------------------------------------
		// ----------------- Render the UI --------------------------
		// ----------------------------------------------------------
		VaadinRenderer renderer = new VaadinRenderer();
		IViewContext context = renderer.render(rootLayout, yView, null);
		IGridEditpart gridEP = DelegatingEditPartManager.getInstance()
				.getEditpart(context, cxGrid);
		GridPresentation gridPres = gridEP.getPresentation();

		Grid grid = gridPres.getWidget();
		assertTrue(gridPres.isRendered());
		assertFalse(gridPres.isDisposed());
		assertTrue(gridEP.isRendered());
		assertFalse(gridEP.isDisposed());

		assertEquals(1, grid.getHeaderRowCount());
		assertEquals(7, grid.getColumns().size());

		Column col = grid.getColumn(P_NAME);
		assertNotNull(col);
		assertNull(col.getConverter());
		assertTrue(col.getRenderer() instanceof TextRenderer);

		col = grid.getColumn(P_NAME2);
		assertNotNull(col);
		assertNull(col.getConverter());
		assertTrue(col.getRenderer() instanceof HtmlRenderer);

		col = grid.getColumn(P_AGE);
		assertNotNull(col);
		assertTrue(col.getConverter() instanceof StringToIntegerConverter);
		assertTrue(col.getRenderer() instanceof ButtonRenderer);

		col = grid.getColumn(P_BIRTHDAY);
		assertNotNull(col);
		assertNull(col.getConverter());
		assertTrue(col.getRenderer() instanceof DateRenderer);

		col = grid.getColumn(P_GENDER);
		assertNotNull(col);
		assertTrue(col.getConverter() instanceof ConverterFactorty.GenderToImageConverter);
		assertTrue(col.getRenderer() instanceof ImageRenderer);

		col = grid.getColumn(P_NATIVE_GERMAN);
		assertNotNull(col);
		assertTrue(col.getConverter() instanceof StringToBooleanConverter);
		assertTrue(col.getRenderer() instanceof TextRenderer);

		col = grid.getColumn(P_VITALITY);
		assertNotNull(col);
		assertNull(col.getConverter());
		assertTrue(col.getRenderer() instanceof ProgressBarRenderer);

		// test editparts
		assertFalse(ModelUtil.getEditpart(context, cxColAge).isDisposed());
		assertFalse(ModelUtil.getEditpart(context, cxColBirthday).isDisposed());
		assertFalse(ModelUtil.getEditpart(context, cxColGender).isDisposed());
		assertFalse(ModelUtil.getEditpart(context, cxColName).isDisposed());
		assertFalse(ModelUtil.getEditpart(context, cxColName2).isDisposed());
		assertFalse(ModelUtil.getEditpart(context, cxColNativeGerman).isDisposed());
		assertFalse(ModelUtil.getEditpart(context, cxColVit).isDisposed());

		gridEP.requestDispose();
		assertFalse(gridPres.isRendered());
		assertTrue(gridPres.isDisposed());
		assertFalse(gridEP.isRendered());
		assertTrue(gridEP.isDisposed());

		assertNull(ModelUtil.findEditpart(cxColAge));
		assertNull(ModelUtil.findEditpart(cxColBirthday));
		assertNull(ModelUtil.findEditpart(cxColGender));
		assertNull(ModelUtil.findEditpart(cxColName));
		assertNull(ModelUtil.findEditpart(cxColName2));
		assertNull(ModelUtil.findEditpart(cxColNativeGerman));
		assertNull(ModelUtil.findEditpart(cxColVit));

	}

	@Test
	public void test_RemoveAddColumns() throws ContextException {

		assertNotNull(VaadinSession.getCurrent());

		// switch the global locale to german
		Locale.setDefault(Locale.GERMAN);

		// ----------------------------------------------------------
		// ------------- Create the layout --------------------------
		// ----------------------------------------------------------
		createDefaultModel();

		// ----------------------------------------------------------
		// ----------------- Render the UI --------------------------
		// ----------------------------------------------------------
		VaadinRenderer renderer = new VaadinRenderer();
		IViewContext context = renderer.render(rootLayout, yView, null);
		IGridEditpart gridEP = DelegatingEditPartManager.getInstance()
				.getEditpart(context, cxGrid);
		GridPresentation gridPres = gridEP.getPresentation();

		Grid grid = gridPres.getWidget();

		Column col = grid.getColumn(P_BIRTHDAY);
		assertNotNull(col);

		// test editor field
		IDateTimeEditpart editorEP = ModelUtil.getEditpart(context, cxColBirthday
				.getEditorField());
		assertFalse(editorEP.isDisposed());
		assertNotNull(editorEP.getWidget());

		// remove column
		IGridColumnEditpart birthdayEP = ModelUtil.getEditpart(context, cxColBirthday);
		cxGrid.getColumns().remove(cxColBirthday);
		assertTrue(birthdayEP.isDisposed());
		assertNull(grid.getColumn(P_BIRTHDAY));

		// test editor
		editorEP = ModelUtil.findEditpart(cxColBirthday.getEditorField());
		assertNull(editorEP);

		// add column
		cxGrid.getColumns().add(2, cxColBirthday);
		birthdayEP = ModelUtil.getEditpart(context, cxColBirthday);
		assertFalse(birthdayEP.isDisposed());
		assertNotNull(grid.getColumn(P_BIRTHDAY));

		gridEP.requestDispose();
	}

	@Test
	public void test_HeaderRows() throws ContextException {

		assertNotNull(VaadinSession.getCurrent());

		// switch the global locale to german
		Locale.setDefault(Locale.GERMAN);

		// ----------------------------------------------------------
		// ------------- Create the layout --------------------------
		// ----------------------------------------------------------
		createExtendedModel();

		// ----------------------------------------------------------
		// ----------------- Render the UI --------------------------
		// ----------------------------------------------------------
		VaadinRenderer renderer = new VaadinRenderer();
		IViewContext context = renderer.render(rootLayout, yView, null);
		IGridEditpart gridEP = DelegatingEditPartManager.getInstance()
				.getEditpart(context, cxGrid);
		GridPresentation gridPres = gridEP.getPresentation();

		Grid grid = gridPres.getWidget();

		assertEquals(3, grid.getHeaderRowCount());

		// remove the header
		IGridHeaderRowEditpart headerRow1EP = ModelUtil
				.getEditpart(context, cxHeaderRow1);
		cxGrid.getHeaders().remove(cxHeaderRow1);
		assertEquals(2, grid.getHeaderRowCount());
		assertTrue(headerRow1EP.isDisposed());

		// add it again
		cxGrid.getHeaders().add(0, cxHeaderRow1);
		headerRow1EP = ModelUtil.findEditpart(cxHeaderRow1);
		assertFalse(headerRow1EP.isDisposed());
		assertEquals(3, grid.getHeaderRowCount());

		// test the meta cell
		HeaderRow headerRow1 = grid.getHeaderRow(1);
		HeaderCell ageCell = headerRow1.getCell(P_AGE);
		assertEquals(GridStaticCellType.WIDGET, ageCell.getCellType());
		CheckBox ageMetaCellComp = (CheckBox) ageCell.getComponent();
		assertNotNull(ageMetaCellComp);

		assertNotNull(ModelUtil.findEditpart(cxCustomCellAge));
		// remove the meta cell
		cxHeaderRow1.getCustomCells().remove(cxCustomCellAge);
		assertNull(ModelUtil.findEditpart(cxCustomCellAge));
		headerRow1 = grid.getHeaderRow(1);
		ageCell = headerRow1.getCell(P_AGE);
		assertEquals(GridStaticCellType.TEXT, ageCell.getCellType());

		// add the meta cell again
		cxHeaderRow1.getCustomCells().add(cxCustomCellAge);
		assertNotNull(ModelUtil.findEditpart(cxCustomCellAge));
		headerRow1 = grid.getHeaderRow(1);
		ageCell = headerRow1.getCell(P_AGE);
		assertEquals(GridStaticCellType.WIDGET, ageCell.getCellType());
		ageMetaCellComp = (CheckBox) ageCell.getComponent();
		assertNotNull(ageMetaCellComp);

		gridEP.requestDispose();
	}

	@Test
	public void test_FooterRows() throws ContextException {

		assertNotNull(VaadinSession.getCurrent());

		// switch the global locale to german
		Locale.setDefault(Locale.GERMAN);

		// ----------------------------------------------------------
		// ------------- Create the layout --------------------------
		// ----------------------------------------------------------
		createExtendedModel();

		// ----------------------------------------------------------
		// ----------------- Render the UI --------------------------
		// ----------------------------------------------------------
		VaadinRenderer renderer = new VaadinRenderer();
		IViewContext context = renderer.render(rootLayout, yView, null);
		IGridEditpart gridEP = DelegatingEditPartManager.getInstance()
				.getEditpart(context, cxGrid);
		GridPresentation gridPres = gridEP.getPresentation();

		Grid grid = gridPres.getWidget();

		assertEquals(2, grid.getFooterRowCount());

		// remove the header
		IGridFooterRowEditpart footerRow1EP = ModelUtil
				.getEditpart(context, cxFooterRowPersonInfo);
		cxGrid.getFooters().remove(cxFooterRowPersonInfo);
		assertEquals(1, grid.getFooterRowCount());
		assertTrue(footerRow1EP.isDisposed());

		// add it again
		cxGrid.getFooters().add(0, cxFooterRowPersonInfo);
		footerRow1EP = ModelUtil.findEditpart(cxFooterRowPersonInfo);
		assertFalse(footerRow1EP.isDisposed());
		assertEquals(2, grid.getFooterRowCount());

		// test the meta cell
		FooterRow footerRow1 = grid.getFooterRow(0);
		FooterCell ageCell = footerRow1.getCell(P_BIRTHDAY);
		assertEquals(GridStaticCellType.WIDGET, ageCell.getCellType());
		NumericField ageMetaCellComp = (NumericField) ageCell.getComponent();
		assertNotNull(ageMetaCellComp);

		assertNotNull(ModelUtil.findEditpart(cxCustomCellBirthdayFooter));
		// remove the meta cell
		cxFooterRowPersonInfo.getCustomCells().remove(
				cxCustomCellBirthdayFooter);
		assertNull(ModelUtil.findEditpart(cxCustomCellBirthdayFooter));
		footerRow1 = grid.getFooterRow(0);
		ageCell = footerRow1.getCell(P_BIRTHDAY);
		assertEquals(GridStaticCellType.TEXT, ageCell.getCellType());

		// add the meta cell again
		cxFooterRowPersonInfo.getCustomCells().add(cxCustomCellBirthdayFooter);
		assertNotNull(ModelUtil.findEditpart(cxCustomCellBirthdayFooter));
		footerRow1 = grid.getFooterRow(0);
		ageCell = footerRow1.getCell(P_BIRTHDAY);
		assertEquals(GridStaticCellType.WIDGET, ageCell.getCellType());
		ageMetaCellComp = (NumericField) ageCell.getComponent();
		assertNotNull(ageMetaCellComp);

		gridEP.requestDispose();
	}

	@Test
	public void test_FilterRow() throws ContextException {

		assertNotNull(VaadinSession.getCurrent());

		// switch the global locale to german
		Locale.setDefault(Locale.GERMAN);

		// ----------------------------------------------------------
		// ------------- Create the layout --------------------------
		// ----------------------------------------------------------
		createExtendedModel();
		cxGrid.setFilteringVisible(true);

		// ----------------------------------------------------------
		// ----------------- Render the UI --------------------------
		// ----------------------------------------------------------
		VaadinRenderer renderer = new VaadinRenderer();
		IViewContext context = renderer.render(rootLayout, yView, null);
		IGridEditpart gridEP = DelegatingEditPartManager.getInstance()
				.getEditpart(context, cxGrid);
		GridPresentation gridPres = gridEP.getPresentation();

		Grid grid = gridPres.getWidget();

		assertEquals(4, grid.getHeaderRowCount());

		HeaderRow filterRow = grid.getHeaderRow(3);
		HeaderCell cell = filterRow.getCell(P_NAME2);
		assertNotNull(cell.getComponent());
		assertTrue(cell.getComponent() instanceof TextSearchField);

		YSearchField cxSearchField = cxColName2.getSearchField();
		assertNotNull(ModelUtil.findEditpart(cxSearchField));

		// remove the header
		cxGrid.setFilteringVisible(false);
		assertEquals(3, grid.getHeaderRowCount());
		assertNull(ModelUtil.findEditpart(cxSearchField));

		// add it again
		cxGrid.setFilteringVisible(true);
		assertEquals(4, grid.getHeaderRowCount());
		assertNotNull(ModelUtil.findEditpart(cxSearchField));

		filterRow = grid.getHeaderRow(3);
		cell = filterRow.getCell(P_NAME2);
		assertNotNull(cell.getComponent());
		assertTrue(cell.getComponent() instanceof TextSearchField);

		// change the search field -> current is disposed
		cxColName2.setSearchField(null);
		assertNull(ModelUtil.findEditpart(cxSearchField));

		// type of cell changed to TEXT
		filterRow = grid.getHeaderRow(3);
		cell = filterRow.getCell(P_NAME2);
		assertEquals(GridStaticCellType.TEXT, cell.getCellType());

		gridEP.requestDispose();
	}

	protected void createDefaultModel() {
		yView = factory.createView();
		cxGrid = cxFactory.createCxGrid();
		yView.setContent(cxGrid);

		cxGrid.setFooterVisible(true);
		cxGrid.setType(Person.class);
		cxGrid.setFilteringVisible(false);
		cxGrid.setSelectionType(YSelectionType.SINGLE);
		cxGrid.setTypeQualifiedName(Person.class.getName());

		CxGridDelegateCellStyleGenerator cxGenerator = CxGridFactory.eINSTANCE
				.createCxGridDelegateCellStyleGenerator();
		cxGenerator.setDelegateId("sampleGrid");
		cxGrid.setCellStyleGenerator(cxGenerator);

		cxColName = cxFactory.createCxGridColumn();
		cxColName.setPropertyId(P_NAME);
		cxColName.setPropertyPath(P_NAME);
		cxColName.setEditable(true);
		cxGrid.getColumns().add(cxColName);

		cxColName2 = cxFactory.createCxGridColumn();
		cxColName2.setPropertyId(P_NAME2);
		cxColName2.setPropertyPath(P_NAME2);
		cxColName2.setEditable(true);
		cxColName2.setRenderer(CxGridRendererFactory.eINSTANCE
				.createCxGridHtmlRenderer());

		YTextSearchField name2SF = ExtensionModelFactory.eINSTANCE
				.createYTextSearchField();
		name2SF.setPropertyPath(P_NAME2);
		cxColName2.setSearchField(name2SF);
		cxGrid.getColumns().add(cxColName2);

		cxColAge = cxFactory.createCxGridColumn();
		cxColAge.setPropertyId(P_AGE);
		cxColAge.setPropertyPath(P_AGE);
		cxColAge.setEditable(true);
		cxAgeRenderer = CxGridRendererFactory.eINSTANCE
				.createCxGridButtonRenderer();
		cxAgeRendererClickEndpoint = cxAgeRenderer
				.createLastClickEventEndpoint();
		cxColAge.setRenderer(cxAgeRenderer);
		YNumericSearchField cxAgeSF = ExtensionModelFactory.eINSTANCE
				.createYNumericSearchField();
		cxAgeSF.setPropertyPath(P_AGE);
		cxAgeSF.setType(Integer.class);
		cxColAge.setSearchField(cxAgeSF);
		cxGrid.getColumns().add(cxColAge);

		cxColNativeGerman = cxFactory.createCxGridColumn();
		cxColNativeGerman.setPropertyId(P_NATIVE_GERMAN);
		cxColNativeGerman.setPropertyPath(P_NATIVE_GERMAN);
		cxColNativeGerman.setEditable(true);
		YBooleanSearchField cxNativeGermanSF = ExtensionModelFactory.eINSTANCE
				.createYBooleanSearchField();
		cxNativeGermanSF.setPropertyPath(P_NATIVE_GERMAN);
		cxColNativeGerman.setSearchField(cxNativeGermanSF);
		cxGrid.getColumns().add(cxColNativeGerman);

		cxColGender = cxFactory.createCxGridColumn();
		cxColGender.setPropertyId(P_GENDER);
		cxColGender.setPropertyPath(P_GENDER);
		cxColGender.setEditable(true);
		cxColGender.setRenderer(CxGridRendererFactory.eINSTANCE
				.createCxGridImageRenderer());
		YDelegateConverter yGenderConverter = CoreModelFactory.eINSTANCE
				.createYDelegateConverter();
		yGenderConverter.setConverterId("genderToImageConverter");
		cxColGender.setConverter(yGenderConverter);
		cxGrid.getColumns().add(cxColGender);

		cxColVit = cxFactory.createCxGridColumn();
		cxColVit.setPropertyId(P_VITALITY);
		cxColVit.setPropertyPath(P_VITALITY);
		cxColVit.setEditable(true);
		cxVitRenderer = CxGridRendererFactory.eINSTANCE
				.createCxGridProgressBarRenderer();
		cxVitRenderer.setMaxValue(250);
		cxColVit.setRenderer(cxVitRenderer);
		cxGrid.getColumns().add(cxColVit);

		cxColBirthday = cxFactory.createCxGridColumn();
		cxColBirthday.setPropertyId(P_BIRTHDAY);
		cxColBirthday.setPropertyPath(P_BIRTHDAY);
		cxColBirthday.setEditable(true);
		YDateTime cxBirthdayEditor = ExtensionModelFactory.eINSTANCE
				.createYDateTime();
		cxColBirthday.setEditorField(cxBirthdayEditor);
		cxBirthdayRenderer = CxGridRendererFactory.eINSTANCE
				.createCxGridDateRenderer();
		cxBirthdayRenderer.setDateFormat("yyyy-MM-dd");
		cxColBirthday.setRenderer(cxBirthdayRenderer);
		cxGrid.getColumns().add(cxColBirthday);
	}

	protected void createExtendedModel() {
		createDefaultModel();

		cxGrid.setCustomFilters(true);
		
		// group name and name2 in header "Names Group"
		//
		cxNamesGroupCell = cxFactory.createCxGridGroupedCell();
		cxNamesGroupCell.setLabel("Names Group");
		cxNamesGroupCell.getGroupables().add(cxColName);
		cxNamesGroupCell.getGroupables().add(cxColName2);

		cxHeaderRow1 = cxFactory.createCxGridHeaderRow();
		cxHeaderRow1.getGroupings().add(cxNamesGroupCell);
		cxGrid.getHeaders().add(cxHeaderRow1);

		cxCustomCellAge = cxFactory.createCxGridMetaCell();
		cxCustomCellAge.setElement(ExtensionModelFactory.eINSTANCE
				.createYCheckBox());
		cxCustomCellAge.setTarget(cxColAge);
		cxHeaderRow1.getCustomCells().add(cxCustomCellAge);

		// group "Names Group" and age in "Person Group"
		//
		cxPersonGroupCell = cxFactory.createCxGridGroupedCell();
		cxPersonGroupCell.setLabel("Person Group");
		cxPersonGroupCell.getGroupables().add(cxNamesGroupCell);
		cxPersonGroupCell.getGroupables().add(cxColAge);

		cxHeaderRow2 = cxFactory.createCxGridHeaderRow();
		cxHeaderRow2.getGroupings().add(cxPersonGroupCell);
		cxGrid.getHeaders().add(cxHeaderRow2);

		cxCustomCellBirthday = cxFactory.createCxGridMetaCell();
		cxCustomCellBirthday.setElement(ExtensionModelFactory.eINSTANCE
				.createYNumericField());
		cxCustomCellBirthday.setTarget(cxColBirthday);
		cxHeaderRow2.getCustomCells().add(cxCustomCellBirthday);

		//
		// footer
		//

		// group name and name2 in header "Names Group"
		//
		cxFooterGroup1 = cxFactory.createCxGridGroupedCell();
		cxFooterGroup1.setLabel("Names Group");
		cxFooterGroup1.getGroupables().add(cxColName);
		cxFooterGroup1.getGroupables().add(cxColName2);

		cxFooterRow = cxFactory.createCxGridFooterRow();
		cxFooterRow.getGroupings().add(cxFooterGroup1);
		cxGrid.getFooters().add(cxFooterRow);

		// group "Names Group" and age in "Person Group"
		//
		cxFooterGroup2 = cxFactory.createCxGridGroupedCell();
		cxFooterGroup2.setLabel("Person Group");
		cxFooterGroup2.getGroupables().add(cxFooterGroup1);
		cxFooterGroup2.getGroupables().add(cxColAge);

		cxFooterRowPersonInfo = cxFactory.createCxGridFooterRow();
		cxFooterRowPersonInfo.getGroupings().add(cxFooterGroup2);
		cxGrid.getFooters().add(cxFooterRowPersonInfo);

		cxCustomCellBirthdayFooter = cxFactory.createCxGridMetaCell();
		cxCustomCellBirthdayFooter.setElement(ExtensionModelFactory.eINSTANCE
				.createYNumericField());
		cxCustomCellBirthdayFooter.setTarget(cxColBirthday);
		cxFooterRowPersonInfo.getCustomCells().add(cxCustomCellBirthdayFooter);
	}

	/**
	 * A simple I18n service implementation.
	 */
	@Component(immediate = true, service = { II18nService.class })
	public static class I18nService extends AbstractDisposable implements
			II18nService {

		@Override
		public String getValue(String i18nKey, Locale locale) {
			return i18nKey;
		}

		@Override
		protected void internalDispose() {
			// nothing to do
		}
	}

}
