package org.eclipse.osbp.runtime.web.sample.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Date;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.Filter;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.eclipse.osbp.runtime.common.annotations.Range;
import org.eclipse.osbp.runtime.web.sample.dtos.AddressDto;
import org.eclipse.osbp.runtime.web.sample.dtos.BaseUUIDDto;
import org.eclipse.osbp.runtime.web.sample.dtos.Gender;

@SuppressWarnings("all")
public class PersonDto extends BaseUUIDDto implements IDto, Serializable, PropertyChangeListener {
  @Filter
  private String firstname;
  
  @Range
  private String lastname;
  
  @Valid
  @Filter
  private Date birthdate;
  
  @Filter
  private int age;
  
  @Range
  private double weight;
  
  private Gender gender;
  
  @DomainReference
  @FilterDepth(depth = 3)
  private AddressDto address;
  
  public PersonDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link Person} to the dto {@link PersonDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the firstname property or <code>null</code> if not present.
   */
  public String getFirstname() {
    return this.firstname;
  }
  
  /**
   * Sets the <code>firstname</code> property to this instance.
   * 
   * @param firstname - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setFirstname(final String firstname) {
    firePropertyChange("firstname", this.firstname, this.firstname = firstname );
  }
  
  /**
   * Returns the lastname property or <code>null</code> if not present.
   */
  public String getLastname() {
    return this.lastname;
  }
  
  /**
   * Sets the <code>lastname</code> property to this instance.
   * 
   * @param lastname - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setLastname(final String lastname) {
    firePropertyChange("lastname", this.lastname, this.lastname = lastname );
  }
  
  /**
   * Returns the birthdate property or <code>null</code> if not present.
   */
  public Date getBirthdate() {
    return this.birthdate;
  }
  
  /**
   * Sets the <code>birthdate</code> property to this instance.
   * 
   * @param birthdate - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setBirthdate(final Date birthdate) {
    firePropertyChange("birthdate", this.birthdate, this.birthdate = birthdate );
  }
  
  /**
   * Returns the age property or <code>null</code> if not present.
   */
  public int getAge() {
    return this.age;
  }
  
  /**
   * Sets the <code>age</code> property to this instance.
   * 
   * @param age - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setAge(final int age) {
    firePropertyChange("age", this.age, this.age = age );
  }
  
  /**
   * Returns the weight property or <code>null</code> if not present.
   */
  public double getWeight() {
    return this.weight;
  }
  
  /**
   * Sets the <code>weight</code> property to this instance.
   * 
   * @param weight - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setWeight(final double weight) {
    firePropertyChange("weight", this.weight, this.weight = weight );
  }
  
  /**
   * Returns the gender property or <code>null</code> if not present.
   */
  public Gender getGender() {
    return this.gender;
  }
  
  /**
   * Sets the <code>gender</code> property to this instance.
   * 
   * @param gender - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setGender(final Gender gender) {
    firePropertyChange("gender", this.gender, this.gender = gender );
  }
  
  /**
   * Returns the address property or <code>null</code> if not present.
   */
  public AddressDto getAddress() {
    return this.address;
  }
  
  /**
   * Sets the <code>address</code> property to this instance.
   * 
   * @param address - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setAddress(final AddressDto address) {
    checkDisposed();
    firePropertyChange("address", this.address, this.address = address);
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
