package org.eclipse.osbp.runtime.web.sample.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.runtime.web.sample.dtos.AddressDto;
import org.eclipse.osbp.runtime.web.sample.entities.Address;

@SuppressWarnings("all")
public class AddressDtoService extends AbstractDTOServiceWithMutablePersistence<AddressDto, Address> {
  public AddressDtoService() {
    // set the default persistence ID
    setPersistenceId("sample");
  }
  
  public Class<AddressDto> getDtoClass() {
    return AddressDto.class;
  }
  
  public Class<Address> getEntityClass() {
    return Address.class;
  }
  
  public Object getId(final AddressDto dto) {
    return dto.getUuid();
  }
}
