package org.eclipse.osbp.runtime.web.sample.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.Filter;
import org.eclipse.osbp.runtime.web.sample.entities.BaseUUID;
import org.eclipse.osbp.runtime.web.sample.entities.Country;

@Entity
@Table(name = "ADDRESS")
@SuppressWarnings("all")
public class Address extends BaseUUID implements IEntity {
  @Column(name = "STREET")
  @Filter
  private String street;
  
  @Column(name = "CITY")
  @Filter
  private String city;
  
  @ManyToOne
  @JoinColumn(name = "COUNTRY_ID")
  private Country country;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the street property or <code>null</code> if not present.
   */
  public String getStreet() {
    checkDisposed();
    return this.street;
  }
  
  /**
   * Sets the street property to this instance.
   */
  public void setStreet(final String street) {
    checkDisposed();
    this.street = street;
  }
  
  /**
   * @return Returns the city property or <code>null</code> if not present.
   */
  public String getCity() {
    checkDisposed();
    return this.city;
  }
  
  /**
   * Sets the city property to this instance.
   */
  public void setCity(final String city) {
    checkDisposed();
    this.city = city;
  }
  
  /**
   * @return Returns the country property or <code>null</code> if not present.
   */
  public Country getCountry() {
    checkDisposed();
    return this.country;
  }
  
  /**
   * Sets the country property to this instance.
   */
  public void setCountry(final Country country) {
    checkDisposed();
    this.country = country;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
