package org.eclipse.osbp.runtime.web.sample.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.Filter;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.eclipse.osbp.runtime.web.sample.dtos.BaseUUIDDto;
import org.eclipse.osbp.runtime.web.sample.dtos.CountryDto;

@SuppressWarnings("all")
public class AddressDto extends BaseUUIDDto implements IDto, Serializable, PropertyChangeListener {
  @Filter
  private String street;
  
  @Filter
  private String city;
  
  @DomainReference
  @FilterDepth(depth = 3)
  private CountryDto country;
  
  public AddressDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link Address} to the dto {@link AddressDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the street property or <code>null</code> if not present.
   */
  public String getStreet() {
    return this.street;
  }
  
  /**
   * Sets the <code>street</code> property to this instance.
   * 
   * @param street - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setStreet(final String street) {
    firePropertyChange("street", this.street, this.street = street );
  }
  
  /**
   * Returns the city property or <code>null</code> if not present.
   */
  public String getCity() {
    return this.city;
  }
  
  /**
   * Sets the <code>city</code> property to this instance.
   * 
   * @param city - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setCity(final String city) {
    firePropertyChange("city", this.city, this.city = city );
  }
  
  /**
   * Returns the country property or <code>null</code> if not present.
   */
  public CountryDto getCountry() {
    return this.country;
  }
  
  /**
   * Sets the <code>country</code> property to this instance.
   * 
   * @param country - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setCountry(final CountryDto country) {
    checkDisposed();
    firePropertyChange("country", this.country, this.country = country);
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
