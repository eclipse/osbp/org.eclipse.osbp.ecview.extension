package org.eclipse.osbp.runtime.web.sample.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.runtime.web.sample.dtos.CountryDto;
import org.eclipse.osbp.runtime.web.sample.entities.Country;

@SuppressWarnings("all")
public class CountryDtoService extends AbstractDTOServiceWithMutablePersistence<CountryDto, Country> {
  public CountryDtoService() {
    // set the default persistence ID
    setPersistenceId("sample");
  }
  
  public Class<CountryDto> getDtoClass() {
    return CountryDto.class;
  }
  
  public Class<Country> getEntityClass() {
    return Country.class;
  }
  
  public Object getId(final CountryDto dto) {
    return dto.getUuid();
  }
}
