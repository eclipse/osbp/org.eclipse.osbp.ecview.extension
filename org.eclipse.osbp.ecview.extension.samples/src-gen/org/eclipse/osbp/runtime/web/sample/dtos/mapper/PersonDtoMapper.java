package org.eclipse.osbp.runtime.web.sample.dtos.mapper;

import java.util.Date;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.runtime.web.sample.dtos.AddressDto;
import org.eclipse.osbp.runtime.web.sample.dtos.Gender;
import org.eclipse.osbp.runtime.web.sample.dtos.PersonDto;
import org.eclipse.osbp.runtime.web.sample.dtos.mapper.BaseUUIDDtoMapper;
import org.eclipse.osbp.runtime.web.sample.entities.Address;
import org.eclipse.osbp.runtime.web.sample.entities.Person;

/**
 * This class maps the dto {@link PersonDto} to and from the entity {@link Person}.
 * 
 */
@SuppressWarnings("all")
public class PersonDtoMapper<DTO extends PersonDto, ENTITY extends Person> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public Person createEntity() {
    return new Person();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public PersonDto createDto() {
    return new PersonDto();
  }
  
  /**
   * Maps the entity {@link Person} to the dto {@link PersonDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final PersonDto dto, final Person entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setFirstname(toDto_firstname(entity, context));
    dto.setLastname(toDto_lastname(entity, context));
    dto.setBirthdate(toDto_birthdate(entity, context));
    dto.setAge(toDto_age(entity, context));
    dto.setWeight(toDto_weight(entity, context));
    dto.setGender(toDto_gender(entity, context));
    dto.setAddress(toDto_address(entity, context));
  }
  
  /**
   * Maps the dto {@link PersonDto} to the entity {@link Person}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final PersonDto dto, final Person entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setFirstname(toEntity_firstname(dto, entity, context));
    entity.setLastname(toEntity_lastname(dto, entity, context));
    entity.setBirthdate(toEntity_birthdate(dto, entity, context));
    entity.setAge(toEntity_age(dto, entity, context));
    entity.setWeight(toEntity_weight(dto, entity, context));
    entity.setGender(toEntity_gender(dto, entity, context));
    entity.setAddress(toEntity_address(dto, entity, context));
  }
  
  /**
   * Maps the property firstname from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_firstname(final Person in, final MappingContext context) {
    return in.getFirstname();
  }
  
  /**
   * Maps the property firstname from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_firstname(final PersonDto in, final Person parentEntity, final MappingContext context) {
    return in.getFirstname();
  }
  
  /**
   * Maps the property lastname from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_lastname(final Person in, final MappingContext context) {
    return in.getLastname();
  }
  
  /**
   * Maps the property lastname from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_lastname(final PersonDto in, final Person parentEntity, final MappingContext context) {
    return in.getLastname();
  }
  
  /**
   * Maps the property birthdate from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Date toDto_birthdate(final Person in, final MappingContext context) {
    return in.getBirthdate();
  }
  
  /**
   * Maps the property birthdate from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Date toEntity_birthdate(final PersonDto in, final Person parentEntity, final MappingContext context) {
    return in.getBirthdate();
  }
  
  /**
   * Maps the property age from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toDto_age(final Person in, final MappingContext context) {
    return in.getAge();
  }
  
  /**
   * Maps the property age from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toEntity_age(final PersonDto in, final Person parentEntity, final MappingContext context) {
    return in.getAge();
  }
  
  /**
   * Maps the property weight from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected double toDto_weight(final Person in, final MappingContext context) {
    return in.getWeight();
  }
  
  /**
   * Maps the property weight from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected double toEntity_weight(final PersonDto in, final Person parentEntity, final MappingContext context) {
    return in.getWeight();
  }
  
  /**
   * Maps the property gender from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Gender toDto_gender(final Person in, final MappingContext context) {
    if(in.getGender() != null) {
    	return org.eclipse.osbp.runtime.web.sample.dtos.Gender.valueOf(in.getGender().name());
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property gender from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected org.eclipse.osbp.runtime.web.sample.entities.Gender toEntity_gender(final PersonDto in, final Person parentEntity, final MappingContext context) {
    if(in.getGender() != null) {
    	return org.eclipse.osbp.runtime.web.sample.entities.Gender.valueOf(in.getGender().name());
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property address from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected AddressDto toDto_address(final Person in, final MappingContext context) {
    if(in.getAddress() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address>) getToDtoMapper(AddressDto.class, in.getAddress().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	AddressDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getAddress()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getAddress(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getAddress(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property address from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected Address toEntity_address(final PersonDto in, final Person parentEntity, final MappingContext context) {
    if(in.getAddress() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address>) getToEntityMapper(in.getAddress().getClass(), Address.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	Address entity = null;
    	entity = context.get(mapper.createEntityHash(in.getAddress()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (Address) context
    			.findEntityByEntityManager(Address.class, in.getAddress().getUuid());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getAddress()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getAddress(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(PersonDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(Person.class, in);
  }
}
