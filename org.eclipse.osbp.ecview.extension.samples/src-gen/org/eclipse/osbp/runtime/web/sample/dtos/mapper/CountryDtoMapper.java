package org.eclipse.osbp.runtime.web.sample.dtos.mapper;

import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.runtime.web.sample.dtos.CountryDto;
import org.eclipse.osbp.runtime.web.sample.dtos.mapper.BaseUUIDDtoMapper;
import org.eclipse.osbp.runtime.web.sample.entities.Country;

/**
 * This class maps the dto {@link CountryDto} to and from the entity {@link Country}.
 * 
 */
@SuppressWarnings("all")
public class CountryDtoMapper<DTO extends CountryDto, ENTITY extends Country> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public Country createEntity() {
    return new Country();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CountryDto createDto() {
    return new CountryDto();
  }
  
  /**
   * Maps the entity {@link Country} to the dto {@link CountryDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CountryDto dto, final Country entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setName(toDto_name(entity, context));
    dto.setIsoCode(toDto_isoCode(entity, context));
    dto.setEuMember(toDto_euMember(entity, context));
  }
  
  /**
   * Maps the dto {@link CountryDto} to the entity {@link Country}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CountryDto dto, final Country entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setName(toEntity_name(dto, entity, context));
    entity.setIsoCode(toEntity_isoCode(dto, entity, context));
    entity.setEuMember(toEntity_euMember(dto, entity, context));
  }
  
  /**
   * Maps the property name from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_name(final Country in, final MappingContext context) {
    return in.getName();
  }
  
  /**
   * Maps the property name from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_name(final CountryDto in, final Country parentEntity, final MappingContext context) {
    return in.getName();
  }
  
  /**
   * Maps the property isoCode from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_isoCode(final Country in, final MappingContext context) {
    return in.getIsoCode();
  }
  
  /**
   * Maps the property isoCode from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_isoCode(final CountryDto in, final Country parentEntity, final MappingContext context) {
    return in.getIsoCode();
  }
  
  /**
   * Maps the property euMember from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_euMember(final Country in, final MappingContext context) {
    return in.getEuMember();
  }
  
  /**
   * Maps the property euMember from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_euMember(final CountryDto in, final Country parentEntity, final MappingContext context) {
    return in.getEuMember();
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CountryDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(Country.class, in);
  }
}
