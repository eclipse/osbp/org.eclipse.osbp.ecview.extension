/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.runtime.web.sample;

import org.eclipse.osbp.ecview.core.common.context.ContextException;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YAlignment;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.util.ECViewUtil;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringType;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayoutCellStyle;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.VaadinRenderer;
import org.eclipse.osbp.runtime.web.sample.dtos.PersonDto;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
@Theme(ValoTheme.THEME_NAME)
public class ECViewSampleUINative extends UI {

	private CssLayout content = new CssLayout();

	// private DeepResolvingBeanItemContainer<Person> container;
	// private FiltersComponent filtersComp;
	private IViewContext viewContext;

	CoreModelFactory coreModelFactory = CoreModelFactory.eINSTANCE;
	ExtensionModelFactory modelFactory = ExtensionModelFactory.eINSTANCE;

	@Override
	protected void init(VaadinRequest request) {
		setContent(content);

		content.setWidth("800px");
		content.setHeight("100%");

		YView yView = createViewModel();

		VaadinRenderer renderer = new VaadinRenderer();
		try {
			viewContext = renderer.render(content, yView, null);
		} catch (ContextException e1) {
			e1.printStackTrace();
		}
	}

	private YView createViewModel() {

		YView view = coreModelFactory.createYView();
		YVerticalLayout content = modelFactory.createYVerticalLayout();
		view.setContent(content);

		YFilteringComponent comp = modelFactory.createYFilteringComponent();
		content.getElements().add(comp);
		YVerticalLayoutCellStyle cellStyle = content.addCellStyle(comp);
		cellStyle.setAlignment(YAlignment.FILL_FILL);

		// ALTERNATIVE 1
		//
		ECViewUtil.fill(PersonDto.class, 3, comp);
		
		// ALTERNATIVE 2
		//
//		comp.setType(PersonDto.class);
//		comp.setTypeQualifiedName(PersonDto.class.getCanonicalName());
//		comp.addFilterDescriptor(YFilteringType.RANGE, "firstname");
//		comp.addFilterDescriptor(YFilteringType.COMPARE, "lastname");
//		comp.addFilterDescriptor(YFilteringType.RANGE, "birthdate");
//		comp.addFilterDescriptor(YFilteringType.RANGE, "weight");
//		comp.addFilterDescriptor(YFilteringType.COMPARE, "age");
//		comp.addFilterDescriptor(YFilteringType.RANGE, "address.street");
//		comp.addFilterDescriptor(YFilteringType.COMPARE, "address.city");
//		comp.addFilterDescriptor(YFilteringType.RANGE, "address.country.name");
//		comp.addFilterDescriptor(YFilteringType.COMPARE, "address.country.isoCode");
//		comp.addFilterDescriptor(YFilteringType.COMPARE, "address.country.euMember");
//
//		comp.addTableDescriptor("firstname");
//		comp.addTableDescriptor("birthdate");
//		comp.addTableDescriptor("weight");
//		comp.addTableDescriptor("age");
//		comp.addTableDescriptor("address.street");
//		comp.addTableDescriptor("address.country.name");
//		comp.addTableDescriptor("address.country.euMember");

		return view;
	}

}
