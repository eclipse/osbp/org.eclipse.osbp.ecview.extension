/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.osbp.runtime.web.sample;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.spi.PersistenceProvider;
import javax.persistence.spi.PersistenceProviderResolver;
import javax.persistence.spi.PersistenceProviderResolverHolder;

import org.eclipse.osbp.runtime.web.sample.entities.Address;
import org.eclipse.osbp.runtime.web.sample.entities.Country;
import org.eclipse.osbp.runtime.web.sample.entities.Person;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

@Component(immediate = true)
public class JPAStartup {

	private Country country_AT;
	private Country country_DE;
	private Country country_CH;
	private Address vienna;
	private Address berlin;
	private Address zurich;
	private Person person_V;
	private Person person_B;
	private Person person_Z;

	protected Map<String, Object> properties = new HashMap<String, Object>();
	protected EntityManagerFactory emf;

	@Activate
	public void activate() throws Exception {
		PersistenceProviderResolverHolder.setPersistenceProviderResolver(new PersistenceProviderResolver() {
			private List<PersistenceProvider> providers = new ArrayList<PersistenceProvider>();

			@Override
			public List<PersistenceProvider> getPersistenceProviders() {
				org.eclipse.persistence.jpa.PersistenceProvider provider = new org.eclipse.persistence.jpa.PersistenceProvider();
				providers.add(provider);
				return providers;
			}

			@Override
			public void clearCachedProviders() {
				providers.clear();
			}
		});
		properties.put(PersistenceUnitProperties.CLASSLOADER, getClass().getClassLoader());

		emf = Persistence.createEntityManagerFactory("sample", properties);
		Bundle bundle = FrameworkUtil.getBundle(JPAStartup.class);

		Dictionary<String, Object> props = new Hashtable<>();
		props.put("osgi.unit.name", "sample");
		bundle.getBundleContext().registerService(EntityManagerFactory.class, emf, props);

		initData();

	}

	private void initData() {

		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		country_AT = createCountry("Austria", "AT", true);
		country_DE = createCountry("Germany", "DE", true);
		country_CH = createCountry("Switzerland", "CH", false);

		vienna = createAddress("Stephansplatz", "Vienna", country_AT);
		berlin = createAddress("Somewhere", "Berlin", country_DE);
		zurich = createAddress("Am See", "Zürich", country_CH);

		person_V = createPerson("Florian", "Pirchner", toDate("1978-10-14"), 61.234, 30, vienna);
		person_B = createPerson("Klemens", "Edler", toDate("1978-02-28"), 83.912, 40, berlin);
		person_Z = createPerson("Jörg", "Riegel", toDate("1969-07-03"), 72.456, 50, zurich);

		em.persist(country_AT);
		em.persist(country_DE);
		em.persist(country_CH);

		em.persist(vienna);
		em.persist(berlin);
		em.persist(zurich);

		em.persist(person_V);
		em.persist(person_B);
		em.persist(person_Z);

		em.getTransaction().commit();
	}

	private Date toDate(String string) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return df.parse(string);
		} catch (ParseException e) {
		}
		return null;
	}

	Country createCountry(String name, String isoCode, boolean euMember) {
		Country c = new Country();
		c.setName(name);
		c.setIsoCode(isoCode);
		c.setEuMember(euMember);
		return c;
	}

	Address createAddress(String street, String city, Country country) {
		Address a = new Address();
		a.setStreet(street);
		a.setCity(city);
		a.setCountry(country);
		return a;
	}

	Person createPerson(String firstname, String lastname, Date birthdate, double weight, int age, Address address) {
		Person a = new Person();
		a.setFirstname(firstname);
		a.setLastname(lastname);
		a.setBirthdate(birthdate);
		a.setWeight(weight);
		a.setAge(age);
		a.setAddress(address);
		return a;
	}
}
