/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.components;

import java.util.Collections;
import java.util.List;

import org.eclipse.osbp.ecview.core.common.editpart.emf.FieldEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;
import org.eclipse.osbp.ecview.extension.editparts.components.IBlobUploadComponentEditpart;
import org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent;

/**
 * The Class BlobUploadComponentEditpart.
 */
public class BlobUploadComponentEditpart extends FieldEditpart<YBlobUploadComponent> implements
		IBlobUploadComponentEditpart {

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.FieldEditpart#getDatatypeValidators()
	 */
	@Override
	public List<IValidatorEditpart> getDatatypeValidators() {
		return Collections.emptyList();
	}
	
}
