/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.util;

import org.eclipse.osbp.ecview.core.util.emf.ModelUtil;
import org.eclipse.osbp.ecview.extension.api.IFocusingEnhancer;
import org.eclipse.osbp.ecview.extension.editparts.IStrategyLayoutEditpart;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;

/**
 * The Class OsECViewUtil.
 */
public class OsECViewUtil extends ModelUtil {
	
	/**
	 * Sets the focusing enhancer.
	 *
	 * @param layout
	 *            the layout
	 * @param focusingEnhancerId
	 *            the focusing enhancer id
	 */
	public void setFocusingEnhancer(YStrategyLayout layout,
			String focusingEnhancerId) {
		IStrategyLayoutEditpart strategyEditpart = ModelUtil
				.findEditpart(layout);
		IFocusingEnhancer enhancer = strategyEditpart
				.findFocusingEnhancerService(focusingEnhancerId);
		layout.getFocusingStrategies().clear();
		enhancer.enhanceFocusing(layout);
	}
}
