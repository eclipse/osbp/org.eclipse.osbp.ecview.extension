/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.strategies;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.extension.api.ILayoutingStrategy;
import org.eclipse.osbp.ecview.extension.editparts.IStrategyLayoutEditpart;
import org.eclipse.osbp.ecview.extension.editparts.strategies.ILayoutingStrategyEditpart;
import org.eclipse.osbp.ecview.extension.model.YECviewFactory;
import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * See {@link ILayoutingStrategyEditpart}.
 *
 * @param <M>
 *            the generic type
 */
public abstract class LayoutingStrategyEditpart<M extends YLayoutingStrategy>
		extends ElementEditpart<M> implements ILayoutingStrategyEditpart {

	/** The Constant LOGGER. */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(LayoutingStrategyEditpart.class);

	/** The strategy. */
	protected ILayoutingStrategy strategy;

	/** The layouting info. */
	private YLayoutingInfo layoutingInfo;

	/** The y view. */
	protected YView yView;

	/**
	 * The default constructor.
	 */
	protected LayoutingStrategyEditpart() {
	}

	
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public IStrategyLayoutEditpart getParent() {
		YElement yParent = (YElement) getModel().eContainer();
		return yParent != null ? (IStrategyLayoutEditpart) getEditpart(getContext(),yParent)	: null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#initialize(org.eclipse.osbp.ecview.core.common.model.core.YElement)
	 */
	public void initialize(IViewContext context, M model) {
		super.initialize(context, model);

		yView = getModel().getView();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.strategies.ILayoutingStrategyEditpart#getView()
	 */
	@Override
	public IViewEditpart getView() {
		return yView != null ? (IViewEditpart) getEditpart(getContext(),yView) : null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void internalDispose() {
		try {
			strategy = null;
			yView = null;
		} finally {
			super.internalDispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#requestDispose()
	 */
	@Override
	public void requestDispose() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Creates a new layouting info.
	 *
	 * @return the y layouting info
	 */
	protected YLayoutingInfo createLayoutingInfo() {
		YLayoutingInfo info = YECviewFactory.eINSTANCE
				.createYLayoutingInfo();
		info.setLayout(getLayout());
		return info;
	}

	/**
	 * Gets the layout.
	 *
	 * @return the layout
	 */
	protected YStrategyLayout getLayout() {
		return (YStrategyLayout) getParent().getModel();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.strategies.ILayoutingStrategyEditpart#layout()
	 */
	@Override
	public void layout() {
		checkDisposed();

		if (strategy != null) {
			layoutingInfo = createLayoutingInfo();
			// reset the layouting info
			getLayout().setLayoutingInfo(null);
			strategy.layout(layoutingInfo);
			// now set the layoutInfo again
			getLayout().setLayoutingInfo(layoutingInfo);
		}
	}

	/**
	 * ONLY FOR TESTS!.
	 *
	 * @return the strategy
	 */
	public ILayoutingStrategy getStrategy() {
		return strategy;
	}

}
