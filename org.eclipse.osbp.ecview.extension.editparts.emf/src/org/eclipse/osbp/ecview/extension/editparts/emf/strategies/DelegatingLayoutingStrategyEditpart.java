/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.strategies;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.extension.api.ILayoutingStrategyProvider;
import org.eclipse.osbp.ecview.extension.editparts.strategies.IDelegatingLayoutingStrategyEditpart;
import org.eclipse.osbp.ecview.extension.editparts.strategies.IFocusingStrategyEditpart;
import org.eclipse.osbp.ecview.extension.model.YDelegatingLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * See {@link IFocusingStrategyEditpart}.
 */
public class DelegatingLayoutingStrategyEditpart extends
		LayoutingStrategyEditpart<YDelegatingLayoutingStrategy> implements
		IDelegatingLayoutingStrategyEditpart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DelegatingLayoutingStrategyEditpart.class);

	/**
	 * The default constructor.
	 */
	protected DelegatingLayoutingStrategyEditpart() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#initialize(org.eclipse.osbp.ecview.core.common.model.core.YElement)
	 */
	@Override
	public void initialize(IViewContext context, YDelegatingLayoutingStrategy model) {
		applyInitialStrategy(model);

		super.initialize(context, model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case YECviewPackage.YDELEGATING_LAYOUTING_STRATEGY__DELEGATE_STRATEGY_ID:
			internalSetDelegate(notification.getNewStringValue());
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}

	/**
	 * Applies the initial strategy and ensures that key strokes are available.
	 *
	 * @param model
	 *            the model
	 */
	protected void applyInitialStrategy(YDelegatingLayoutingStrategy model) {
		ILayoutingStrategyProvider result = findService(model
				.getDelegateStrategyId());
		if (result != null) {
			this.strategy = result.getStrategy();
		} else {
			LOGGER.warn("Strategy was null. So current strategy was not replaced.");
		}
	}

	/**
	 * Internal set delegate.
	 *
	 * @param id
	 *            the id
	 */
	public void internalSetDelegate(String id) {
		checkDisposed();

		ILayoutingStrategyProvider result = findService(id);
		if (result != null) {
			this.strategy = result.getStrategy();
		} else {
			LOGGER.warn("Strategy was null. So current strategy was not replaced.");
		}
	}

	/**
	 * Finds the service with the given id.
	 *
	 * @param id
	 *            the id
	 * @return the i layouting strategy provider
	 */
	private ILayoutingStrategyProvider findService(String id) {
		BundleContext context = FrameworkUtil.getBundle(getClass())
				.getBundleContext();

		try {
			Collection<ServiceReference<ILayoutingStrategyProvider>> references = context
					.getServiceReferences(ILayoutingStrategyProvider.class,
							String.format("(ecview.layouting.id=%s)", id));
			if (!references.isEmpty()) {
				ILayoutingStrategyProvider provider = context
						.getService(references.iterator().next());
				return provider;
			}
		} catch (InvalidSyntaxException e) {
			LOGGER.warn("{}", e);
		}
		return null;
	}

	// @Override
	// public void handleEvent(Event event) {
	// checkDisposed();
	//
	// ILayoutingStrategyProvider result = (ILayoutingStrategyProvider) event
	// .getProperty(IEventBroker.DATA);
	// if (result != null) {
	// this.strategy = result.getStrategy();
	//
/**
	// getParent().requestLayouting();
	// } else {
	// LOGGER.warn("Strategy was null. So current strategy was not replaced.");
	// }
	// }

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.emf.strategies.LayoutingStrategyEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			
		} finally {
			super.internalDispose();
		}
	}

}
