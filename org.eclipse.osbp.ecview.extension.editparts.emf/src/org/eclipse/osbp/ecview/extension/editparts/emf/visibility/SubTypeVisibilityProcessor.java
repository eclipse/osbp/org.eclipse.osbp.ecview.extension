/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.visibility;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityManager;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor;

/**
 * The Class SubTypeVisibilityProcessor.
 */
public class SubTypeVisibilityProcessor implements IVisibilityProcessor {

	/** The input. */
	@SuppressWarnings("unused")
	private Object input;

	/** The manager. */
	private IVisibilityManager manager;

	private YSubTypeVisibilityProcessor yVP;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor#init
	 * (org.eclipse.osbp.ecview.core.common.visibility.IVisibilityManager)
	 */
	@Override
	public void init(IVisibilityManager manager) {
		this.manager = manager;
	}

	/**
	 * Setup.
	 *
	 * @param context
	 *            the context
	 * @param yVP
	 *            the y vp
	 */
	public void setup(IViewContext context, YSubTypeVisibilityProcessor yVP) {
		this.yVP = yVP;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor#fire
	 * ()
	 */
	@Override
	public void fire() {
		IVisibilityHandler handler = manager.getById(yVP.getTarget().getId());
		if (handler != null) {
			if (yVP.matchesType(input != null ? input.getClass() : null)) {
				handler.setVisible(true);
			} else {
				handler.setVisible(false);
			}
			handler.apply();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor#setInput
	 * (java.lang.Object)
	 */
	@Override
	public void setInput(Object input) {
		this.input = input;

		fire();
	}
}
