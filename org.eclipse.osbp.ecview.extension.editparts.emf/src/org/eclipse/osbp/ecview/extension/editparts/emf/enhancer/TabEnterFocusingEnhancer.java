/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.enhancer;

import org.eclipse.osbp.ecview.extension.api.IFocusingEnhancer;
import org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YECviewFactory;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.osgi.service.component.annotations.Component;

/**
 * The Class TabEnterFocusingEnhancer.
 */
@Component(property={"ecview.focusing.enhancer.id=TabEnterFocusingEnhancer"})
public class TabEnterFocusingEnhancer implements IFocusingEnhancer {

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.api.IFocusingEnhancer#enhanceFocusing(org.eclipse.osbp.ecview.extension.model.YStrategyLayout)
	 */
	@Override
	public void enhanceFocusing(YStrategyLayout cxLayout) {
		cxLayout.getFocusingStrategies().clear();
		
		YDelegatingFocusingStrategy yStgy1 = YECviewFactory.eINSTANCE.createYDelegatingFocusingStrategy();
		yStgy1.setDelegateStrategyId("cx.enter.backward");
		cxLayout.getFocusingStrategies().add(yStgy1);
		
		YDelegatingFocusingStrategy yStgy2 = YECviewFactory.eINSTANCE.createYDelegatingFocusingStrategy();
		yStgy2.setDelegateStrategyId("cx.tab.forward");
		cxLayout.getFocusingStrategies().add(yStgy2);
		
	}

}
