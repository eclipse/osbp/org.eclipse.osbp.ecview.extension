/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YFocusable;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.extension.editparts.IInternalLayoutingInfoEditpart;
import org.eclipse.osbp.ecview.extension.editparts.IInternalSuspectLayoutingInfoEditpart;
import org.eclipse.osbp.ecview.extension.editparts.IStrategyLayoutEditpart;
import org.eclipse.osbp.ecview.extension.editparts.ISuspectEditpart;
import org.eclipse.osbp.ecview.extension.editparts.ISuspectLayoutingInfoEditpart;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link ISuspectEditpart}.
 */
public class LayoutingInfoEditpart extends ElementEditpart<YLayoutingInfo>
		implements IInternalLayoutingInfoEditpart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(LayoutingInfoEditpart.class);

	/** The suspect infos. */
	private List<ISuspectLayoutingInfoEditpart> suspectInfos;
	
	/** The content. */
	private IEmbeddableEditpart content;

	/** The active. */
	private boolean active;

	/**
	 * A default constructor.
	 */
	protected LayoutingInfoEditpart() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelAdd(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelAdd(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case YECviewPackage.YLAYOUTING_INFO__ACTIVE_SUSPECT_INFOS:
			YSuspectInfo ySuspectLayoutingInfo = (YSuspectInfo) notification
					.getNewValue();

			ISuspectLayoutingInfoEditpart editPart = (ISuspectLayoutingInfoEditpart) getEditpart(getContext(), ySuspectLayoutingInfo);
			internalAddSuspectLayoutingInfo(editPart);
			break;
		default:
			break;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelRemove(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelRemove(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case YECviewPackage.YLAYOUTING_INFO__ACTIVE_SUSPECT_INFOS:
			YSuspectInfo ySuspectLayoutingInfo = (YSuspectInfo) notification
					.getOldValue();

			ISuspectLayoutingInfoEditpart editPart = (ISuspectLayoutingInfoEditpart) getEditpart(getContext(),ySuspectLayoutingInfo);
			internalRemoveSuspectLayoutingInfo(editPart);

			break;
		default:
			break;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case YECviewPackage.YLAYOUTING_INFO__CONTENT:
			internalSetContent((YEmbeddable) notification.getNewValue());
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}

	/**
	 * Ensures that the internal bindings are loaded properly.
	 */
	private void ensureSuspectLayoutingInfosLoaded() {
		if (suspectInfos == null) {
			internalLoadSuspectLayoutingInfos();
		}
	}

	/**
	 * Is called to load and initialize all bindings.
	 */
	protected void internalLoadSuspectLayoutingInfos() {
		checkDisposed();

		if (suspectInfos == null) {
			suspectInfos = new ArrayList<ISuspectLayoutingInfoEditpart>();
			for (YSuspectInfo yBindingEP : getModel().getActiveSuspectInfos()) {
				ISuspectLayoutingInfoEditpart editPart = getEditpart(getContext(),yBindingEP);
				internalAddSuspectLayoutingInfo(editPart);
			}
		}
	}

	/**
	 * Is called to change the internal state and add the given editpart to the
	 * list of bindings.
	 * 
	 * @param editpart
	 *            The editpart to be added
	 */
	protected void internalAddSuspectLayoutingInfo(
			ISuspectLayoutingInfoEditpart editpart) {
		checkDisposed();

		ensureSuspectLayoutingInfosLoaded();
		if (!suspectInfos.contains(editpart)) {
			suspectInfos.add(editpart);
		}
	}

	/**
	 * Is called to change the internal state and remove the given editpart from
	 * the list of bindings.
	 * 
	 * @param editpart
	 *            The editpart to be removed
	 */
	protected void internalRemoveSuspectLayoutingInfo(
			ISuspectLayoutingInfoEditpart editpart) {
		checkDisposed();

		if (suspectInfos != null && editpart != null) {
			editpart.dispose();
			suspectInfos.remove(editpart);
		}
	}

	/**
	 * Gets the suspect layouting infos.
	 *
	 * @return the suspect layouting infos
	 */
	public List<ISuspectLayoutingInfoEditpart> getSuspectLayoutingInfos() {
		ensureSuspectLayoutingInfosLoaded();
		return Collections.unmodifiableList(suspectInfos);
	}

	/**
	 * Set the new content.
	 *
	 * @param yEmbeddable
	 *            the y embeddable
	 */
	protected void internalSetContent(YEmbeddable yEmbeddable) {
		ensureContentLoaded();

		IEmbeddableEditpart newContent = getEditpart(getContext(),yEmbeddable);

		if (this.content == newContent) {
			return;
		}

		IEmbeddableEditpart oldContent = this.content;
		this.content = newContent;

		if (oldContent != null) {
			oldContent.dispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {

			LOGGER.info("Disposing " + toString());

			active = false;
			// lazy loading: edit parts also have to be disposed if they have
			// not been loaded yet, but exist in the model.
			if (suspectInfos != null
					|| getModel().getActiveSuspectInfos().size() > 0) {
				List<ISuspectLayoutingInfoEditpart> tempElements = getSuspectLayoutingInfos();
				synchronized (suspectInfos) {
					for (ISuspectLayoutingInfoEditpart editpart : tempElements
							.toArray(new ISuspectLayoutingInfoEditpart[tempElements
									.size()])) {
						editpart.dispose();
					}
				}
			}
			suspectInfos = null;

			if (content != null) {
				content.dispose();
				content = null;
			}

		} finally {
			super.internalDispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.ILayoutingInfoEditpart#getView()
	 */
	public IViewEditpart getView() {
		YView yView = ((YStrategyLayout) getModel().eContainer()).getView();
		return yView != null ? (IViewEditpart) getEditpart(getContext(),yView) : null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.ILayoutingInfoEditpart#getParent()
	 */
	@Override
	public IStrategyLayoutEditpart getParent() {
		YStrategyLayout cxLayout = (YStrategyLayout) getModel().eContainer();
		return cxLayout != null ? (IStrategyLayoutEditpart) getEditpart(getContext(),cxLayout)
				: null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.ILayoutingInfoEditpart#getContent()
	 */
	@Override
	public IEmbeddableEditpart getContent() {
		ensureContentLoaded();
		return content;
	}

	/**
	 * Ensures that the internal bindings are loaded properly.
	 */
	private void ensureContentLoaded() {
		if (content == null) {
			content = getEditpart(getContext(),getModel().getContent());
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.IInternalLayoutingInfoEditpart#activate()
	 */
	@Override
	public void activate() {
		if (active) {
			return;
		}

		checkDisposed();
		try {
			for (ISuspectLayoutingInfoEditpart suspect : getSuspectLayoutingInfos()) {
				((IInternalSuspectLayoutingInfoEditpart) suspect).activate();
			}

			// set the first field to be focused
			if (getModel().getFirstFocus() != null) {
				YView yView = (YView) getView().getModel();
				YEmbeddable yTarget = getModel().getFirstFocus().getTarget();
				if (yTarget != null && yTarget instanceof YFocusable) {
					yView.setCurrentFocus((YFocusable) yTarget);
				}
			}

		} finally {
			active = true;
		}
	}
	
	@Override
	public void postActivate() {
		for (ISuspectLayoutingInfoEditpart suspect : getSuspectLayoutingInfos()) {
			((IInternalSuspectLayoutingInfoEditpart) suspect).postActivate();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#requestDispose()
	 */
	@Override
	public void requestDispose() {
		if (getParent() == null) {
			return;
		}
		getParent().deactivate(this);
	}
}
