/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.components;

import org.eclipse.osbp.ecview.core.common.editpart.emf.FieldEditpart;
import org.eclipse.osbp.ecview.extension.editparts.components.II18nComboBoxEditpart;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YI18nComboBox;

/**
 * The implementation of the IUiComboBoxEditpart.
 */
public class I18nComboBoxEditpart extends FieldEditpart<YI18nComboBox>
		implements II18nComboBoxEditpart {

	public I18nComboBoxEditpart() {
		super(YECviewPackage.Literals.YI1_8N_COMBO_BOX__DATATYPE);
	}

}
