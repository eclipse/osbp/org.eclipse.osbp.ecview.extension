/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.services.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.eclipse.osbp.ecview.core.common.extender.IECViewCache;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.sample.item.dtos.TelevisionDto;
import org.junit.Before;
import org.junit.Test;
import org.knowhowlab.osgi.testing.utils.BundleUtils;
import org.knowhowlab.osgi.testing.utils.ServiceUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

public class ECViewCacheBuilderTest {

	private BundleContext bc;
	private IECViewCache cache;

	@Before
	public void setup() throws Exception {
		bc = getBundleContext();
		BundleUtils.startBundleAsync(bc, "org.eclipse.osbp.ecview.extension.services");
		BundleUtils.startBundleAsync(bc,
				"org.eclipse.osbp.ecview.extension.editparts.emf");
		BundleUtils.startBundleAsync(bc,
				"org.eclipse.osbp.ecview.extension.presentation.vaadin");

		cache = ServiceUtils.getService(getBundleContext(), IECViewCache.class);
		assertNotNull(cache);
	}

	private BundleContext getBundleContext() {
		return FrameworkUtil.getBundle(getClass()).getBundleContext();
	}

	@Test
	public void test_getById() {

		YView result = cache.getView("org.my");
		assertNotNull(result);
		assertEquals("org.my", result.getId());
	}

	@Test
	public void test_getByClass() {
		List<YView> result = cache.getViews(TelevisionDto.class, "dsTV");
		assertEquals(1, result.size());
		assertEquals("org.my", result.get(0).getId());
	}

}
