/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.services.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.extender.IECViewProviderService;
import org.eclipse.osbp.sample.item.dtos.TelevisionDto;
import org.junit.Before;
import org.junit.Test;
import org.knowhowlab.osgi.testing.utils.BundleUtils;
import org.knowhowlab.osgi.testing.utils.ServiceUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

public class ECViewProviderServiceTest {

	private BundleContext bc;
	private IECViewProviderService service;

	@Before
	public void setup() throws Exception {
		bc = getBundleContext();
		BundleUtils.startBundleAsync(bc, "org.eclipse.osbp.ecview.extension.services");
		BundleUtils.startBundleAsync(bc,
				"org.eclipse.osbp.ecview.extension.editparts.emf");
		BundleUtils.startBundleAsync(bc,
				"org.eclipse.osbp.ecview.extension.presentation.vaadin");

		service = ServiceUtils.getService(getBundleContext(),
				IECViewProviderService.class);
		assertNotNull(service);
	}

	private BundleContext getBundleContext() {
		return FrameworkUtil.getBundle(getClass()).getBundleContext();
	}

	@Test
	public void test_getById() {
		IViewContext result = service.getViewContext(TelevisionDto.class, "dsTV");
		assertNotNull(result);
		assertEquals(TelevisionDto.class, result.getBean("dsTV"));

		IViewContext result2 = service.getViewContext(TelevisionDto.class, "dsTV");
		assertNotSame(result, result2);
	}

	@Test
	public void test_getByClass() {
		IViewContext result = service.getViewContext("org.eclipse.osbp.foodmart.uimodels.TV");
		assertNotNull(result);
		assertEquals(TelevisionDto.class, result.getBean("dsTV"));

		IViewContext result2 = service.getViewContext("org.eclipse.osbp.foodmart.uimodels.TV");
		assertNotSame(result, result2);
	}

}
