/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.sample.item.dtos;

import org.eclipse.osbp.sample.item.dtos.DetailsDto;
import org.eclipse.osbp.sample.item.dtos.TelevionType;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@SuppressWarnings("all")
public class TelevisionDetailsDto extends DetailsDto implements IDto, Serializable, PropertyChangeListener {
  private int size;
  
  private boolean threeD;
  
  private TelevionType type;
  
/**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
/**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
/**
   * Returns the size property or <code>null</code> if not present.
   */
  public int getSize() {
    return this.size;
  }
  
/**
   * Sets the <code>size</code> property to this instance.
   * 
   * @param size - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setSize(final int size) {
    firePropertyChange("size", this.size, this.size = size );
  }
  
/**
   * Returns the threeD property or <code>null</code> if not present.
   */
  public boolean getThreeD() {
    return this.threeD;
  }
  
/**
   * Sets the <code>threeD</code> property to this instance.
   * 
   * @param threeD - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setThreeD(final boolean threeD) {
    firePropertyChange("threeD", this.threeD, this.threeD = threeD );
  }
  
/**
   * Returns the type property or <code>null</code> if not present.
   */
  public TelevionType getType() {
    return this.type;
  }
  
/**
   * Sets the <code>type</code> property to this instance.
   * 
   * @param type - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setType(final TelevionType type) {
    firePropertyChange("type", this.type, this.type = type );
  }
  
  public TelevisionDetailsDto createDto() {
    return new TelevisionDetailsDto();
  }
  
}
