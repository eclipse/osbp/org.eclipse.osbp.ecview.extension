/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.sample;

import java.io.InputStream;
import java.util.Locale;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YDelegateConverter;
import org.eclipse.osbp.ecview.core.common.presentation.IConverterFactory;
import org.osgi.service.component.annotations.Component;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;

/**
 * The Class ConverterFactorty.
 */
@Component(immediate = true)
public class ConverterFactorty implements IConverterFactory {

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IConverterFactory#isFor(org.eclipse.osbp.ecview.core.common.context.IViewContext, org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart)
	 */
	@Override
	public boolean isFor(IViewContext uiContext, IConverterEditpart editpart) {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IConverterFactory#createConverter(org.eclipse.osbp.ecview.core.common.context.IViewContext, org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart)
	 */
	@Override
	public Object createConverter(IViewContext uiContext,
			IConverterEditpart editpart) throws IllegalArgumentException {
		YDelegateConverter yConverter = (YDelegateConverter) editpart
				.getModel();
		if (yConverter.getConverterId().equals("genderToImageConverter")) {
			return new GenderToImageConverter();
		}

		return null;
	}

	/**
	 * The Class GenderToImageConverter.
	 */
	@SuppressWarnings("serial")
	private static class GenderToImageConverter implements
			Converter<Resource, Gender> {

		/* (non-Javadoc)
		 * @see com.vaadin.data.util.converter.Converter#convertToModel(java.lang.Object, java.lang.Class, java.util.Locale)
		 */
		@Override
		public Gender convertToModel(Resource value,
				Class<? extends Gender> targetType, Locale locale)
				throws com.vaadin.data.util.converter.Converter.ConversionException {
			return null;
		}

		/* (non-Javadoc)
		 * @see com.vaadin.data.util.converter.Converter#convertToPresentation(java.lang.Object, java.lang.Class, java.util.Locale)
		 */
		@Override
		public Resource convertToPresentation(Gender value,
				Class<? extends Resource> targetType, Locale locale)
				throws com.vaadin.data.util.converter.Converter.ConversionException {

			switch (value) {
			case FEMALE:
				return new ThemeResource("gridsample/female.gif");
			case MALE:
				return new ThemeResource("gridsample/male.gif");
			}
			return null;
		}

		/* (non-Javadoc)
		 * @see com.vaadin.data.util.converter.Converter#getModelType()
		 */
		@Override
		public Class<Gender> getModelType() {
			return Gender.class;
		}

		/* (non-Javadoc)
		 * @see com.vaadin.data.util.converter.Converter#getPresentationType()
		 */
		@Override
		public Class<Resource> getPresentationType() {
			return Resource.class;
		}

	}

}
