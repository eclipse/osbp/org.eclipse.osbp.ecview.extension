/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.sample;

import org.eclipse.osbp.runtime.web.vaadin.databinding.VaadinObservables;

import com.vaadin.server.DeploymentConfiguration;
import com.vaadin.server.ServiceException;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinServletService;
import com.vaadin.ui.UI;

/**
 * The Class ServletService.
 */
@SuppressWarnings("serial")
public class ServletService extends VaadinServletService {

	/**
	 * Instantiates a new servlet service.
	 *
	 * @param servlet
	 *            the servlet
	 * @param deploymentConfiguration
	 *            the deployment configuration
	 * @throws ServiceException
	 *             the service exception
	 */
	public ServletService(VaadinServlet servlet,
			DeploymentConfiguration deploymentConfiguration)
			throws ServiceException {
		super(servlet, deploymentConfiguration);
	}

	/* (non-Javadoc)
	 * @see com.vaadin.server.VaadinService#getClassLoader()
	 */
	@Override
	public ClassLoader getClassLoader() {
		// return the bundle classloader
		// see http://dev.vaadin.com/ticket/15516
		return ServletService.class.getClassLoader();
	}

	/* (non-Javadoc)
	 * @see com.vaadin.server.VaadinService#findUI(com.vaadin.server.VaadinRequest)
	 */
	public UI findUI(VaadinRequest request) {
		UI instance = super.findUI(request);

		// activate the realm for the current ui and thread
		VaadinObservables.activateRealm(instance);

		return instance;
	}

}
