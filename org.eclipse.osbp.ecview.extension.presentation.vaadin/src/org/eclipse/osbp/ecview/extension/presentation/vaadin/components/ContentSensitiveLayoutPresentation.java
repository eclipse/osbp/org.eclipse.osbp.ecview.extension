/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.presentation.vaadin.components;

import java.util.ArrayList;
import java.util.Locale;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.ILayoutEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.extension.model.YContentSensitiveLayout;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.IConstants;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractLayoutPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;

import fi.jasoft.dragdroplayouts.DDVerticalLayout;

public class ContentSensitiveLayoutPresentation extends
		AbstractLayoutPresenter<ComponentContainer> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ContentSensitiveLayoutPresentation.class);

	/** The vertical layout. */
	private DDVerticalLayout verticalLayout;

	/** The model access. */
	private ModelAccess modelAccess;

	/**
	 * The constructor.
	 * 
	 * @param editpart
	 *            The editpart of that presentation.
	 */
	public ContentSensitiveLayoutPresentation(IElementEditpart editpart) {
		super((ILayoutEditpart) editpart);
		this.modelAccess = new ModelAccess(
				(YContentSensitiveLayout) editpart.getModel());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.
	 * AbstractVaadinWidgetPresenter#doUpdateLocale(java.util.Locale)
	 */
	@Override
	protected void doUpdateLocale(Locale locale) {
		// no need to set the locale to the ui elements. Is handled by vaadin
		// internally.

		// update the captions
		applyCaptions();
	}

	/**
	 * Applies the labels to the widgets.
	 */
	protected void applyCaptions() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.
	 * AbstractLayoutPresenter
	 * #updateCellStyle(org.eclipse.osbp.ecview.core.common
	 * .model.core.YEmbeddable)
	 */
	public void updateCellStyle(YEmbeddable child) {
		// refresh the whole ui
		refreshUI();
	}

	/**
	 * Is called to refresh the UI. The element will be removed from the grid
	 * layout and added to it again afterwards.
	 */
	protected void refreshUI() {
		verticalLayout.removeAllComponents();

		for (IEmbeddableEditpart child : getChildren()) {
			Component childComponent = (Component) child.render(verticalLayout);
			childComponent.setSizeFull();
			verticalLayout.addComponent(childComponent);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.
	 * AbstractVaadinWidgetPresenter#doCreateWidget(java.lang.Object)
	 */
	@Override
	public ComponentContainer doCreateWidget(Object parent) {
		if (verticalLayout == null) {
			verticalLayout = new DDVerticalLayout();
			setupComponent(verticalLayout, getCastedModel());

			associateWidget(verticalLayout, modelAccess.yLayout);

			if (modelAccess.isCssIdValid()) {
				verticalLayout.setId(modelAccess.getCssID());
			} else {
				verticalLayout.setId(getEditpart().getId());
			}

			if (modelAccess.isMargin()) {
				verticalLayout.addStyleName(IConstants.CSS_CLASS_MARGIN);
				verticalLayout.setMargin(true);
			}

			if (!modelAccess.isSpacing()) {
				verticalLayout.setSpacing(false);
			} else {
				verticalLayout.addStyleName(IConstants.CSS_CLASS_SPACING);
				verticalLayout.setSpacing(true);
			}

			if (modelAccess.isCssClassValid()) {
				verticalLayout.addStyleName(modelAccess.getCssClass());
			} else {
				verticalLayout.addStyleName(CSS_CLASS_CONTROL);
			}
			verticalLayout.addStyleName(IConstants.CSS_CLASS_VERTICALLAYOUT);

			// creates the binding for the field
			createBindings(modelAccess.yLayout, verticalLayout, null);

			// initialize all children
			initializeChildren();

			// and now render children
			renderChildren(false);
		}

		return verticalLayout;
	}

	/**
	 * Adds the children to the superclass and prevents rendering.
	 */
	private void initializeChildren() {
		setRenderLock(true);
		try {
			for (IEmbeddableEditpart editPart : getEditpart().getElements()) {
				super.add(editPart);
			}
		} finally {
			setRenderLock(false);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation#
	 * getWidget()
	 */
	@Override
	public ComponentContainer getWidget() {
		return verticalLayout;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation#
	 * isRendered()
	 */
	@Override
	public boolean isRendered() {
		return verticalLayout != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.
	 * AbstractLayoutPresenter#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			for (IEmbeddableEditpart child : new ArrayList<IEmbeddableEditpart>(
					getChildren())) {
				child.dispose();
			}
			unrender();
		} finally {
			super.internalDispose();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.
	 * AbstractVaadinWidgetPresenter#doUnrender()
	 */
	@Override
	public void doUnrender() {
		if (verticalLayout != null) {

			// unbind all active bindings
			unbind();

			// remove assocations
			unassociateWidget(verticalLayout);

			// unrender the children
			unrenderChildren();

			verticalLayout.removeAllComponents();
			verticalLayout = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.
	 * AbstractLayoutPresenter
	 * #internalAdd(org.eclipse.osbp.ecview.core.common.editpart
	 * .IEmbeddableEditpart)
	 */
	@Override
	protected void internalAdd(IEmbeddableEditpart editpart) {
		refreshUI();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.
	 * AbstractLayoutPresenter
	 * #internalRemove(org.eclipse.osbp.ecview.core.common
	 * .editpart.IEmbeddableEditpart)
	 */
	@Override
	protected void internalRemove(IEmbeddableEditpart child) {
		child.unrender();
		refreshUI();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.presentation.ILayoutPresentation#
	 * renderChildren(boolean)
	 */
	@Override
	public void renderChildren(boolean force) {
		if (force) {
			unrenderChildren();
		}

		refreshUI();
	}

	/**
	 * Will unrender all children.
	 */
	protected void unrenderChildren() {
		for (IEmbeddableEditpart editpart : getChildren()) {
			if (editpart.isRendered()) {
				editpart.unrender();
			}
		}
	}

	/**
	 * An internal helper class.
	 */
	private static class ModelAccess {

		/** The y layout. */
		private final YContentSensitiveLayout yLayout;

		/**
		 * Instantiates a new model access.
		 *
		 * @param yLayout
		 *            the y layout
		 */
		public ModelAccess(YContentSensitiveLayout yLayout) {
			super();
			this.yLayout = yLayout;
		}

		/**
		 * Gets the css class.
		 *
		 * @return the css class
		 * @see org.eclipse.osbp.ecview.core.ui.core.model.core.YCssAble#getCssClass()
		 */
		public String getCssClass() {
			return yLayout.getCssClass();
		}

		/**
		 * Returns true, if the css class is not null and not empty.
		 *
		 * @return true, if is css class valid
		 */
		public boolean isCssClassValid() {
			return getCssClass() != null && !getCssClass().equals("");
		}

		/**
		 * Checks if is spacing.
		 *
		 * @return true, if is spacing
		 * @see org.eclipse.osbp.ecview.core.ui.core.model.extension.YContentSensitiveLayout#isSpacing()
		 */
		public boolean isSpacing() {
			return yLayout.isSpacing();
		}

		/**
		 * Gets the css id.
		 *
		 * @return the css id
		 * @see org.eclipse.osbp.ecview.core.ui.core.model.core.YCssAble#getCssID()
		 */
		public String getCssID() {
			return yLayout.getCssID();
		}

		/**
		 * Returns true, if the css id is not null and not empty.
		 *
		 * @return true, if is css id valid
		 */
		public boolean isCssIdValid() {
			return getCssID() != null && !getCssID().equals("");
		}

		/**
		 * Checks if is margin.
		 *
		 * @return true, if is margin
		 * @see org.eclipse.osbp.ecview.core.ui.core.model.extension.YContentSensitiveLayout#isMargin()
		 */
		public boolean isMargin() {
			return yLayout.isMargin();
		}
	}
}
