/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.presentation.vaadin.components;

import java.util.Locale;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableSelectionEndpoint;
import org.eclipse.osbp.ecview.core.databinding.emf.model.ECViewModelBindable;
import org.eclipse.osbp.ecview.extension.editparts.components.II18nComboBoxEditpart;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YI18nComboBox;
import org.eclipse.osbp.preferences.LocaleUtils;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractFieldWidgetPresenter;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.internal.util.Util;
import org.eclipse.osbp.ui.api.themes.EnumCssClass;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService.ThemeResourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.Field;
import com.vaadin.ui.UI;

/**
 * This presenter is responsible to render a text field on the given layout.
 */
public class I18nComboBoxPresentation extends AbstractFieldWidgetPresenter<Component> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(I18nComboBoxPresentation.class);

	/** The model access. */
	private final ModelAccess modelAccess;

	/** The combo. */
	private ComboBox combo;

	/** The property. */
	private ObjectProperty<String> property;

	/**
	 * Constructor.
	 * 
	 * @param editpart
	 *            The editpart of that presenter
	 */
	public I18nComboBoxPresentation(IElementEditpart editpart) {
		super((II18nComboBoxEditpart) editpart);
		this.modelAccess = new ModelAccess((YI18nComboBox) editpart.getModel());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Component doCreateWidget(Object parent) {
		if (combo == null) {
			// Get Map from FunctionLibraryDSL
			IThemeResourceService themeResourceService = getViewContext().getService(IThemeResourceService.class.getName());
			combo = new ComboBox();
//			combo.addStyleName(CSS_CLASS_CONTROL);
			combo.addStyleName(EnumCssClass.COMBOBOX_WITH_ICON.styleName());
			combo.setImmediate(true);
			combo.setItemCaptionMode(ItemCaptionMode.EXPLICIT_DEFAULTS_ID);
//			combo.setItemCaptionMode(ItemCaptionMode.ICON_ONLY);
			combo.setTextInputAllowed(false);
			combo.setNullSelectionAllowed(false);
			
			setupComponent(combo, getCastedModel());

			associateWidget(combo, modelAccess.yI18nComboBox);
			if (modelAccess.isCssIdValid()) {
				combo.setId(modelAccess.getCssID());
			} else {
				combo.setId(getEditpart().getId());
			}

			// Datasource
			IndexedContainer datasource = new IndexedContainer();
			combo.setContainerDataSource(datasource);
			property = new ObjectProperty<String>("", String.class);
			combo.setPropertyDataSource(property);

			boolean needCountry = false;
			if (UI.getCurrent().getLocale().getCountry().length() == 0) {
				needCountry = true;
			}
			Locale[] locales = Locale.getAvailableLocales();
			// --- sort by display name used in the respective locale, if it is
			// configured ---
			LocaleUtils.sortLocalesOnDisplayName(locales);
			for (Locale locale : locales) {
				// only allow locales with country due to currency sign formatters
				if (locale.getCountry().length() > 0) {
					String loc = locale.toLanguageTag();
					if (needCountry && locale.getLanguage().equals(UI.getCurrent().getLocale().getLanguage())) {
						// force a default country if not already given by default
						Locale newLocale = new Locale(locale.getLanguage(), locale.getCountry());
						UI.getCurrent().setLocale(newLocale);
						Locale.setDefault(newLocale);
						needCountry = false;
					}
					if (ProductConfiguration.getLanguages().containsKey(locale.getLanguage())) {
						// --- set the display name used in the respective
						// locale, if it is configured ---
						combo.setItemCaption(loc, LocaleUtils.getDisplayName(locale));
						datasource.addItem(loc);
						if (themeResourceService != null) {
							combo.setItemIcon(loc, themeResourceService.getThemeResource(locale.getCountry().toLowerCase(), ThemeResourceType.FLAG));
						}
					}
				}
			}


			// creates the binding for the field
			createBindings(modelAccess.yI18nComboBox, combo);

			if (modelAccess.isCssClassValid()) {
				combo.addStyleName(modelAccess.getCssClass());
			}

			applyCaptions();

			initializeField(combo);

		}
		return combo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractVaadinWidgetPresenter#doUpdateLocale(java.util.Locale)
	 */
	@Override
	protected void doUpdateLocale(Locale locale) {
		// no need to set the locale to the ui elements. Is handled by vaadin
		// internally.

		// update the captions
		applyCaptions();
	}

	/**
	 * Applies the labels to the widgets.
	 */
	protected void applyCaptions() {
		Util.applyCaptions(getI18nService(), modelAccess.getLabel(), modelAccess.getLabelI18nKey(), getLocale(), combo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractFieldWidgetPresenter#doGetField()
	 */
	@Override
	protected Field<?> doGetField() {
		return combo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractVaadinWidgetPresenter#internalGetObservableEndpoint(org.eclipse.osbp.ecview.core.common.model.core
	 * .YEmbeddableBindingEndpoint)
	 */
	@Override
	protected IObservable internalGetObservableEndpoint(YEmbeddableBindingEndpoint bindableValue) {
		if (bindableValue == null) {
			throw new IllegalArgumentException("BindableValue must not be null!");
		}

		if (bindableValue instanceof YEmbeddableSelectionEndpoint) {
			return internalGetSelectionEndpoint((YEmbeddableSelectionEndpoint) bindableValue);
		}
		throw new IllegalArgumentException("Not a valid input: " + bindableValue);
	}

	/**
	 * Returns the observable to observe the selection.
	 *
	 * @param yEndpoint
	 *            the y endpoint
	 * @return the i observable value
	 */
	protected IObservableValue internalGetSelectionEndpoint(YEmbeddableSelectionEndpoint yEndpoint) {

		String attributePath = ECViewModelBindable.getAttributePath(YECviewPackage.Literals.YI1_8N_COMBO_BOX__SELECTION, yEndpoint.getAttributePath());

		// return the observable value for text
		return ECViewModelBindable.observeValue(castEObject(getModel()), attributePath, modelAccess.yI18nComboBox.getType(), modelAccess.yI18nComboBox.getEmfNsURI());
	}

	/**
	 * Creates the bindings for the given values.
	 *
	 * @param yCombo
	 *            the y combo
	 * @param combo
	 *            the combo
	 */
	protected void createBindings(YI18nComboBox yCombo, ComboBox combo) {
		// ATTENTION!!!!
		// The creation of the model container binding from widget to
		// ECView-model as in the ComboBoxPresentation
		// (registerBinding(createBindings_ContainerContents...)) is in
		// this case not required due to the existing fixed content collection
		// on the creation of the icon comboBox.

		// create the model binding from widget to ECView-model
		registerBinding(createBindingsSelection(castEObject(getModel()), YECviewPackage.Literals.YI1_8N_COMBO_BOX__SELECTION, combo, yCombo.getType()));

		super.createBindings(yCombo, combo, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation#getWidget()
	 */
	@Override
	public Component getWidget() {
		return combo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation#isRendered()
	 */
	@Override
	public boolean isRendered() {
		return combo != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doUnrender() {
		if (combo != null) {

			// unbind all active bindings
			unbind();

			Component parent = ((Component) combo.getParent());
			if (parent != null && parent instanceof ComponentContainer) {
				((ComponentContainer) parent).removeComponent(combo);
			}

			// remove assocations
			unassociateWidget(combo);

			combo = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void internalDispose() {
		try {
			unrender();
		} finally {
			super.internalDispose();
		}
	}

	/**
	 * A helper class.
	 */
	private static class ModelAccess {

		/** The y icon combo box. */
		private final YI18nComboBox yI18nComboBox;

		/**
		 * Instantiates a new model access.
		 *
		 * @param YI18nComboBox
		 *            the y icon combo box
		 */
		public ModelAccess(YI18nComboBox YI18nComboBox) {
			super();
			this.yI18nComboBox = YI18nComboBox;
		}

		/**
		 * Gets the css class.
		 *
		 * @return the css class
		 * @see org.eclipse.osbp.ecview.core.ui.core.model.core.YCssAble#getCssClass()
		 */
		public String getCssClass() {
			return yI18nComboBox.getCssClass();
		}

		/**
		 * Returns true, if the css class is not null and not empty.
		 *
		 * @return true, if is css class valid
		 */
		public boolean isCssClassValid() {
			return getCssClass() != null && !getCssClass().equals("");
		}

		/**
		 * Gets the css id.
		 *
		 * @return the css id
		 * @see org.eclipse.osbp.ecview.core.ui.core.model.core.YCssAble#getCssID()
		 */
		public String getCssID() {
			return yI18nComboBox.getCssID();
		}

		/**
		 * Returns true, if the css id is not null and not empty.
		 *
		 * @return true, if is css id valid
		 */
		public boolean isCssIdValid() {
			return getCssID() != null && !getCssID().equals("");
		}

		/**
		 * Returns the label.
		 *
		 * @return the label
		 */
		public String getLabel() {
			return yI18nComboBox.getDatadescription() != null ? yI18nComboBox.getDatadescription().getLabel() : null;
			// return YI18nComboBox.getLabel();
		}

		/**
		 * Returns the label.
		 *
		 * @return the label i18n key
		 */
		public String getLabelI18nKey() {
			return yI18nComboBox.getDatadescription() != null ? yI18nComboBox.getDatadescription().getLabelI18nKey() : null;
			// return YI18nComboBox.getLabelI18nKey();
		}
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public EMap<String, String> getProperties() {
		if (modelAccess.yI18nComboBox != null) {
			return modelAccess.yI18nComboBox.getProperties();
		}
		return new BasicEMap<String, String>();
	}

}
