/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.presentation.vaadin.converter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Currency;
import java.util.Locale;

import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.osbp.ecview.core.common.services.IServiceRegistry;
import org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter;
import org.eclipse.osbp.runtime.web.vaadin.components.converter.DecimalDoubleConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SimpleDecimalConverter.
 */
@SuppressWarnings("serial")
public class SimpleDecimalConverter extends DecimalDoubleConverter {

	private static final String CURRENCY_MASKED = "¤";

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SimpleDecimalConverter.class);

	/** The registry. */
	private IServiceRegistry registry;

	/** The y converter. */
	private YSimpleDecimalConverter yConverter;

	private boolean hasCurrencySymbol;

	private String blankBefore = "";

	private String blankAfter = "";

	private int currencyIndex;

	/**
	 * Instantiates a new simple decimal converter.
	 *
	 * @param registry
	 *            the registry
	 * @param yConverter
	 *            the y converter
	 */
	public SimpleDecimalConverter(IServiceRegistry registry,
			YSimpleDecimalConverter yConverter) {
		this.registry = registry;
		this.yConverter = yConverter;
	}

	/**
	 * Instantiates a new simple decimal converter.
	 */
	public SimpleDecimalConverter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.runtime.web.vaadin.components.converter.DecimalConverter
	 * #getFormat(java.util.Locale)
	 */
	@Override
	protected DecimalFormat getFormat(Locale locale) {
		blankBefore = "";
		blankAfter = "";
		if (locale == null) {
			locale = Locale.getDefault();
		}
		DecimalFormat result;
		try {
			currencyIndex = yConverter.getNumberFormatPattern().indexOf(CURRENCY_MASKED);
			if(currencyIndex > -1) {
				hasCurrencySymbol = true;
				if(currencyIndex > 1 && yConverter.getNumberFormatPattern().charAt(currencyIndex-1) == ' ') {
					blankBefore = " ";
				}
				if(currencyIndex < yConverter.getNumberFormatPattern().length()-1 && yConverter.getNumberFormatPattern().charAt(currencyIndex+CURRENCY_MASKED.length()) == ' ') {
					blankAfter = " ";
				}
			}
			result = new DecimalFormat(StringEscapeUtils.unescapeHtml(yConverter.getNumberFormatPattern()),
					DecimalFormatSymbols.getInstance(locale));
		} catch (IllegalArgumentException e) {
			String msg = String.format(
					"formatter %s is invalid for decimal numbers: %s",
					yConverter.getNumberFormatPattern(), e.getLocalizedMessage());
			throw new ConversionException(msg);
		}
		return result;
	}

	@Override
	protected Number convertToNumber(String value,
            Class<? extends Number> targetType, Locale locale)
            throws ConversionException {
        if (value == null) {
            return null;
        }

        // Remove leading and trailing white space
        value = value.trim();

        DecimalFormat format = getFormat(locale);
        // add currency sign if not given to avoid parsing errors
        if(hasCurrencySymbol && !value.contains(format.getCurrency().getSymbol(locale))) {
			if(currencyIndex < yConverter.getNumberFormatPattern().length()-3) {
	        	value = String.format("%s%s%s%s", format.getCurrency().getSymbol(locale), blankAfter, value, blankBefore);
			} else {
				value = String.format("%s%s%s%s", blankAfter, value, blankBefore, format.getCurrency().getSymbol(locale));
			}
        }
        
        // Parse and detect errors. If the full string was not used, it is
        // an error.
        ParsePosition parsePosition = new ParsePosition(0);
        Number parsedValue = format.parse(value, parsePosition);
        if (parsePosition.getIndex() != value.length()) {
            throw new ConversionException("Could not convert '" + value
                    + "' to " + getModelType().getName());
        }

        if (parsedValue == null) {
            // Convert "" to null
            return null;
        }

        return parsedValue;
    }
}
