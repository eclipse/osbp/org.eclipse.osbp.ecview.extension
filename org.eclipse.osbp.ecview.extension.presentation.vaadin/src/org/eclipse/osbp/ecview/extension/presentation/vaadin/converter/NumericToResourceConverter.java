/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.presentation.vaadin.converter;

import java.util.Locale;

import org.eclipse.osbp.ecview.core.common.services.IServiceRegistry;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConverter;
import org.eclipse.osbp.runtime.web.vaadin.common.resource.IResourceProvider;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.server.Resource;

/**
 * The Class NumericToResourceConverter.
 */
@SuppressWarnings("serial")
public class NumericToResourceConverter implements Converter<Resource, Number> {

	/** The cx converter. */
	private YNumericToResourceConverter cxConverter;
	
	/** The registry. */
	private IServiceRegistry registry;

	/**
	 * Instantiates a new numeric to resource converter.
	 *
	 * @param registry
	 *            the registry
	 * @param cxConverter
	 *            the cx converter
	 */
	public NumericToResourceConverter(IServiceRegistry registry,
			YNumericToResourceConverter cxConverter) {
		this.registry = registry;
		this.cxConverter = cxConverter;
	}

	/* (non-Javadoc)
	 * @see com.vaadin.data.util.converter.Converter#convertToModel(java.lang.Object, java.lang.Class, java.util.Locale)
	 */
	@Override
	public Number convertToModel(Resource value,
			Class<? extends Number> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		throw new com.vaadin.data.util.converter.Converter.ConversionException(
				"NumericToResourceConverter is readonly!");
	}

	/* (non-Javadoc)
	 * @see com.vaadin.data.util.converter.Converter#convertToPresentation(java.lang.Object, java.lang.Class, java.util.Locale)
	 */
	@Override
	public Resource convertToPresentation(Number value,
			Class<? extends Resource> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {

		IResourceProvider provider = registry
				.getService(IResourceProvider.class.getName());

		for (YNumericToResourceConfig config : cxConverter.getConfigs()) {
			if (isMatch(value, config)) {
				if (isValid(config.getResourceThemePath())) {
					if (provider != null) {
						return provider.getResource(config
								.getResourceThemePath());
					}
				}

				throw new IllegalStateException("No image defined for "
						+ config);
			}
		}

		return null;
	}

	/**
	 * Checks if is valid.
	 *
	 * @param iconKey
	 *            the icon key
	 * @return true, if is valid
	 */
	private boolean isValid(String iconKey) {
		return iconKey != null && !iconKey.equals("");
	}

	/**
	 * Checks if is match.
	 *
	 * @param value
	 *            the value
	 * @param config
	 *            the config
	 * @return true, if is match
	 */
	protected boolean isMatch(Number value, YNumericToResourceConfig config) {
		double ref = config.getValue();
		double val = value.doubleValue();

		switch (config.getCompare()) {
		case EQUAL:
			return val == ref;
		case GREATER_EQUAL:
			return val >= ref;
		case GREATER_THAN:
			return val > ref;
		case LOWER_EQUAL:
			return val <= ref;
		case LOWER_THAN:
			return val < ref;
		case NOT_EQUAL:
			return val != ref;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.vaadin.data.util.converter.Converter#getModelType()
	 */
	@Override
	public Class<Number> getModelType() {
		return Number.class;
	}

	/* (non-Javadoc)
	 * @see com.vaadin.data.util.converter.Converter#getPresentationType()
	 */
	@Override
	public Class<Resource> getPresentationType() {
		return Resource.class;
	}

}
