/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.presentation.vaadin.components;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableSelectionEndpoint;
import org.eclipse.osbp.ecview.core.databinding.emf.model.ECViewModelBindable;
import org.eclipse.osbp.ecview.extension.editparts.components.IIconComboBoxEditpart;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YIconComboBox;
import org.eclipse.osbp.ecview.extension.vaadin.components.utils.FunctionWrapper;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractFieldWidgetPresenter;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.internal.util.Util;
import org.eclipse.osbp.ui.api.functionlibrary.IFunctionLibraryService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService.ThemeResourceType;
import org.eclipse.osbp.utils.functionnormalizer.api.FunctionTypingAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.server.Resource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.Field;

/**
 * This presenter is responsible to render a text field on the given layout.
 */
public class IconComboBoxPresentation extends AbstractFieldWidgetPresenter<Component> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(IconComboBoxPresentation.class);

	/** The model access. */
	private final ModelAccess modelAccess;

	/** The combo. */
	private ComboBox combo;

	/** The property. */
	private ObjectProperty<String> property;

	/**
	 * Constructor.
	 * 
	 * @param editpart
	 *            The editpart of that presenter
	 */
	public IconComboBoxPresentation(IElementEditpart editpart) {
		super((IIconComboBoxEditpart) editpart);
		this.modelAccess = new ModelAccess((YIconComboBox) editpart.getModel());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Component doCreateWidget(Object parent) {
		if (combo == null) {
			// Get Map from FunctionLibraryDSL
			Map<String, String> comboBoxContent = getComboBoxContent();
			IThemeResourceService themeResourceService = getViewContext().getService(IThemeResourceService.class.getName());
			combo = new ComboBox();
			combo.addStyleName(CSS_CLASS_CONTROL);
			combo.setImmediate(true);
			setupComponent(combo, getCastedModel());

			associateWidget(combo, modelAccess.yIconComboBox);
			if (modelAccess.isCssIdValid()) {
				combo.setId(modelAccess.getCssID());
			} else {
				combo.setId(getEditpart().getId());
			}

			Set<String> iconKeys = comboBoxContent.keySet();
			// Datasource
			combo.setContainerDataSource(new IndexedContainer(iconKeys));
			property = new ObjectProperty<String>("", String.class);
			combo.setPropertyDataSource(property);

			// Image
			for (Entry<String, String> entry : comboBoxContent.entrySet()) {
				Resource icon = themeResourceService.getThemeResource(entry.getValue(), ThemeResourceType.ICON);
				combo.setItemIcon(entry.getKey(), icon);
			}

			combo.setItemCaptionMode(ItemCaptionMode.ICON_ONLY);
			combo.setTextInputAllowed(false);

			// creates the binding for the field
			createBindings(modelAccess.yIconComboBox, combo);

			if (modelAccess.isCssClassValid()) {
				combo.addStyleName(modelAccess.getCssClass());
			}

			applyCaptions();

			initializeField(combo);

		}
		return combo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractVaadinWidgetPresenter#doUpdateLocale(java.util.Locale)
	 */
	@Override
	protected void doUpdateLocale(Locale locale) {
		// no need to set the locale to the ui elements. Is handled by vaadin
		// internally.

		// update the captions
		applyCaptions();
	}

	/**
	 * Applies the labels to the widgets.
	 */
	protected void applyCaptions() {
		Util.applyCaptions(getI18nService(), modelAccess.getLabel(), modelAccess.getLabelI18nKey(), getLocale(), combo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractFieldWidgetPresenter#doGetField()
	 */
	@Override
	protected Field<?> doGetField() {
		return combo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractVaadinWidgetPresenter#internalGetObservableEndpoint(org.eclipse.osbp.ecview.core.common.model.core
	 * .YEmbeddableBindingEndpoint)
	 */
	@Override
	protected IObservable internalGetObservableEndpoint(YEmbeddableBindingEndpoint bindableValue) {
		if (bindableValue == null) {
			throw new IllegalArgumentException("BindableValue must not be null!");
		}

		if (bindableValue instanceof YEmbeddableSelectionEndpoint) {
			return internalGetSelectionEndpoint((YEmbeddableSelectionEndpoint) bindableValue);
		}
		throw new IllegalArgumentException("Not a valid input: " + bindableValue);
	}

	/**
	 * Returns the observable to observe the selection.
	 *
	 * @param yEndpoint
	 *            the y endpoint
	 * @return the i observable value
	 */
	protected IObservableValue internalGetSelectionEndpoint(YEmbeddableSelectionEndpoint yEndpoint) {

		String attributePath = ECViewModelBindable.getAttributePath(YECviewPackage.Literals.YICON_COMBO_BOX__SELECTION, yEndpoint.getAttributePath());

		// return the observable value for text
		return ECViewModelBindable.observeValue(castEObject(getModel()), attributePath, modelAccess.yIconComboBox.getType(), modelAccess.yIconComboBox.getEmfNsURI());
	}

	/**
	 * Creates the bindings for the given values.
	 *
	 * @param yCombo
	 *            the y combo
	 * @param combo
	 *            the combo
	 */
	protected void createBindings(YIconComboBox yCombo, ComboBox combo) {
		// ATTENTION!!!!
		// The creation of the model container binding from widget to
		// ECView-model as in the ComboBoxPresentation
		// (registerBinding(createBindings_ContainerContents...)) is in
		// this case not required due to the existing fixed content collection
		// on the creation of the icon comboBox.

		// create the model binding from widget to ECView-model
		registerBinding(createBindingsSelection(castEObject(getModel()), YECviewPackage.Literals.YICON_COMBO_BOX__SELECTION, combo, yCombo.getType()));

		super.createBindings(yCombo, combo, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation#getWidget()
	 */
	@Override
	public Component getWidget() {
		return combo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation#isRendered()
	 */
	@Override
	public boolean isRendered() {
		return combo != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doUnrender() {
		if (combo != null) {

			// unbind all active bindings
			unbind();

			Component parent = ((Component) combo.getParent());
			if (parent != null && parent instanceof ComponentContainer) {
				((ComponentContainer) parent).removeComponent(combo);
			}

			// remove assocations
			unassociateWidget(combo);

			combo = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void internalDispose() {
		try {
			unrender();
		} finally {
			super.internalDispose();
		}
	}

	/**
	 * A helper class.
	 */
	private static class ModelAccess {

		/** The y icon combo box. */
		private final YIconComboBox yIconComboBox;

		/**
		 * Instantiates a new model access.
		 *
		 * @param yIconComboBox
		 *            the y icon combo box
		 */
		public ModelAccess(YIconComboBox yIconComboBox) {
			super();
			this.yIconComboBox = yIconComboBox;
		}

		/**
		 * Gets the css class.
		 *
		 * @return the css class
		 * @see org.eclipse.osbp.ecview.core.ui.core.model.core.YCssAble#getCssClass()
		 */
		public String getCssClass() {
			return yIconComboBox.getCssClass();
		}

		/**
		 * Returns true, if the css class is not null and not empty.
		 *
		 * @return true, if is css class valid
		 */
		public boolean isCssClassValid() {
			return getCssClass() != null && !getCssClass().equals("");
		}

		/**
		 * Gets the css id.
		 *
		 * @return the css id
		 * @see org.eclipse.osbp.ecview.core.ui.core.model.core.YCssAble#getCssID()
		 */
		public String getCssID() {
			return yIconComboBox.getCssID();
		}

		/**
		 * Returns true, if the css id is not null and not empty.
		 *
		 * @return true, if is css id valid
		 */
		public boolean isCssIdValid() {
			return getCssID() != null && !getCssID().equals("");
		}

		/**
		 * Returns the label.
		 *
		 * @return the label
		 */
		public String getLabel() {
			return yIconComboBox.getDatadescription() != null ? yIconComboBox.getDatadescription().getLabel() : null;
			// return yIconComboBox.getLabel();
		}

		/**
		 * Returns the label.
		 *
		 * @return the label i18n key
		 */
		public String getLabelI18nKey() {
			return yIconComboBox.getDatadescription() != null ? yIconComboBox.getDatadescription().getLabelI18nKey() : null;
			// return yIconComboBox.getLabelI18nKey();
		}
	}

	// ++++++++++++++ FunctionLibraryHelperMethods +++++++++++++++

	/**
	 * Gets the combo box content.
	 *
	 * @return the combo box content
	 */
	private Map<String, String> getComboBoxContent() {
		FunctionTypingAPI functionTypingAPI = new FunctionTypingAPI();
		LinkedHashMap<String, String> value = new LinkedHashMap<>();
		for (Entry<String, String> entry : getProperties()) {
			if (functionTypingAPI.getFunctionImagePickerTypeName().equalsIgnoreCase(entry.getKey())) {

				String functionFqn = entry.getValue();
				FunctionWrapper wrapper = new FunctionWrapper(functionFqn);
				try {
					IFunctionLibraryService service = getViewContext().getService(IFunctionLibraryService.class.getName());
					if (service == null) {
						LOGGER.error("No IFunctionLibraryService available");
						return value;
					}
					@SuppressWarnings("unchecked")
					Map<String, String> callResult = (Map<String, String>) service.invoke(wrapper.getClassName(), wrapper.getMethodName());
					if (callResult != null) {
						return callResult;
					} else {
						LOGGER.error("Incorrect function class or method name.");
						return value;
					}
				} catch (ClassCastException ex) {
					// TODO (JCD): Translation
					LOGGER.error("Return type of the function call '" + functionFqn + "' is not a " + Double.class.getSimpleName() + "!");
				}
			}
		}
		return value;
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public EMap<String, String> getProperties() {
		if (modelAccess.yIconComboBox != null) {
			return modelAccess.yIconComboBox.getProperties();
		}
		return new BasicEMap<String, String>();
	}

}
