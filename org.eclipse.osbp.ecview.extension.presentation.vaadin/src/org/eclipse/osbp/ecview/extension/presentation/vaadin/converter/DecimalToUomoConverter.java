/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.presentation.vaadin.converter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.osbp.ecview.core.common.services.IServiceRegistry;
import org.eclipse.osbp.ecview.extension.model.converter.YDecimalToUomoConverter;
import org.eclipse.osbp.runtime.web.vaadin.components.converter.DecimalDoubleConverter;

//import org.eclipse.osbp.runtime.web.vaadin.components.converter.NumbericConverter;

/**
 * The Class DecimalToUomoConverter.
 */
@SuppressWarnings("serial")
public class DecimalToUomoConverter extends DecimalDoubleConverter {
	// public class NumericToUomoConverter implements Converter<String, Number>
	// {

	/** The registry. */
	private IServiceRegistry registry;

	/** The cx converter. */
	private YDecimalToUomoConverter cxConverter;

	/**
	 * Instantiates a new decimal to uomo converter.
	 *
	 * @param registry
	 *            the registry
	 * @param cxConverter
	 *            the cx converter
	 */
	public DecimalToUomoConverter(IServiceRegistry registry,
			YDecimalToUomoConverter cxConverter) {
		this.registry = registry;
		this.cxConverter = cxConverter;
	}

	// @Override
	// public Double convertToModel(String value,
	// Class<? extends Double> targetType, Locale locale)
	// throws com.vaadin.data.util.converter.Converter.ConversionException {
	// return super.convertToModel(value.replace(" cm", ""), targetType,
	// locale);
	// }
	//
	// @Override
	// public String convertToPresentation(Double value,
	// Class<? extends String> targetType, Locale locale)
	// throws com.vaadin.data.util.converter.Converter.ConversionException {
	//
	// // for (YStringToResourceConfig config : cxConverter.getConfigs()) {
	// // if (isMatch(value, config)) {
	// // IResourceProvider provider = registry
	// // .getService(IResourceProvider.class.getName());
	// // return provider.getResource(config.getResourceURL());
	// // }
	// // }
	//
	// return super.convertToPresentation(value, targetType, locale) + " cm";
	//
	// }

	// /**
	// * Returns the default value for default format.
	// *
	// * @return
	// */
	// protected String getDefaultFormat() {
	// return "##,## cm";
	// }
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.runtime.web.vaadin.components.converter.DecimalConverter
	 * #getFormat(java.util.Locale)
	 */
	//
	@Override
	protected NumberFormat getFormat(Locale locale) {
		DecimalFormat result;
		try {
			if (Locale.GERMANY.equals(locale)) {
				result = new DecimalFormat(StringEscapeUtils.unescapeHtml(super.getNumberFormatPattern()
						+ " cm"),
						DecimalFormatSymbols.getInstance(Locale.GERMAN));
			} else {
				result = new DecimalFormat(StringEscapeUtils.unescapeHtml(super.getNumberFormatPattern()
						+ " ft"), DecimalFormatSymbols.getInstance(Locale.US));
			}
		} catch (IllegalArgumentException e) {
			String msg = String.format(
					"formatter %s is invalid for decimal numbers: %s",
					super.getNumberFormatPattern(), e.getLocalizedMessage());
			throw new ConversionException(msg);
		}

		return result;
	}

}
