/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.presentation.vaadin.utils;

import java.text.DecimalFormat;

/**
 * The Class TempDecimalFormatWrapper.
 */
public class TempDecimalFormatWrapper {
	
	/** The value. */
	private Double value;
	
	/** The format. */
	private DecimalFormat format;
	
	/**
	 * Instantiates a new temp decimal format wrapper.
	 *
	 * @param value
	 *            the value
	 * @param format
	 *            the format
	 */
	public TempDecimalFormatWrapper(Double value, DecimalFormat format) {
		super();
		this.value = value;
		this.format = format;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public Double getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value
	 *            the new value
	 */
	public void setValue(Double value) {
		this.value = value;
	}
	
	/**
	 * Gets the format.
	 *
	 * @return the format
	 */
	public DecimalFormat getFormat() {
		return format;
	}
	
	/**
	 * Sets the format.
	 *
	 * @param format
	 *            the new format
	 */
	public void setFormat(DecimalFormat format) {
		this.format = format;
	}

}
