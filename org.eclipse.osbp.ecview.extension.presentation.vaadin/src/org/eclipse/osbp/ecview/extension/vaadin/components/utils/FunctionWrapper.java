/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.vaadin.components.utils;

/**
 * The Class FunctionWrapper.
 */
public class FunctionWrapper {
	
	/** The class name. */
	private String className;
	
	/** The method name. */
	private String methodName;
	
	/**
	 * Instantiates a new function wrapper.
	 *
	 * @param fqn
	 *            the fqn
	 */
	public FunctionWrapper(String fqn) {
		int lastDot = fqn.lastIndexOf('.');
		className = fqn.substring(0, lastDot);
		methodName = fqn.substring(lastDot + 1);
	}
	
	/**
	 * Gets the class name.
	 *
	 * @return the class name
	 */
	public String getClassName() {
		return className;
	}
	
	/**
	 * Gets the method name.
	 *
	 * @return the method name
	 */
	public String getMethodName() {
		return methodName;
	}
}
