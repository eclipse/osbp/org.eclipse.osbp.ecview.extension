/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.editparts.emf;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.emf.EmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YMemento;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.presentation.IGridPresentation;
import org.eclipse.osbp.ecview.extension.grid.util.CxGridUtil;
import org.eclipse.osbp.runtime.common.memento.IMementoHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GridEditpart.
 */
public class GridEditpart extends EmbeddableEditpart<CxGrid> implements
		IGridEditpart {

	/** The Constant LOGGER. */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GridEditpart.class);

	/** The grid content adapter. */
	private GridEditpartController gridContentAdapter;

	/**
	 * Returns the gridEditpart if it could be found in the parent hierarchy of
	 * the given eObject.
	 *
	 * @param context
	 *            the view context
	 * @param eObject
	 *            the e object
	 * @return the grid
	 */
	public static IGridEditpart getGrid(IViewContext context, EObject eObject) {
		CxGrid cxGrid = CxGridUtil.getGrid(eObject);
		if (cxGrid != null) {
			return getEditpart(context, cxGrid);
		}

		return null;
	}

	/**
	 * Returns the grid presentation if available.
	 *
	 * @param context
	 *            the view context
	 * @param eObject
	 *            the e object
	 * @return the grid presentation
	 */
	public static IGridPresentation<?> getGridPresentation(
			IViewContext context, EObject eObject) {
		IGridEditpart editpart = getGrid(context, eObject);
		return (IGridPresentation<?>) (editpart != null ? editpart
				.getPresentation() : null);
	}

	/**
	 * Instantiates a new grid editpart.
	 */
	protected GridEditpart() {

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#createModel()
	 */
	@Override
	public void initialize(IViewContext context, CxGrid model) {
		super.initialize(context, model);

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.EmbeddableEditpart#initialize(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable)
	 */
		// attach the notification dispatcher
		gridContentAdapter = new GridEditpartController(context);
		model.eAdapters().add(gridContentAdapter);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.grid.editparts.IGridEditpart#setFilter(java.util.Map)
	 */
	@Override
	public void setFilter(Map<String, Object> filter) {
		if (isPresentationPresent()) {
			getPresentation().setFilter(filter);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.EmbeddableEditpart#render(java.lang.Object)
	 */
	@Override
	public Object render(Object parentWidget) {
		return super.render(parentWidget);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.datatypes.IDatatypeEditpart.DatatypeBridge#getDatatypeValidators()
	 */
	@Override
	public List<IValidatorEditpart> getDatatypeValidators() {
		return Collections.emptyList();
	}

	/**
	 * Returns true, if the presenter is present.
	 *
	 * @return true, if is presentation present
	 */
	protected boolean isPresentationPresent() {
		return internalGetPresentation() != null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.EmbeddableEditpart#getPresentation()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public IGridPresentation<?> getPresentation() {
		return super.getPresentation();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.EmbeddableEditpart#doInitPresentation(org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation)
	 */
	@Override
	protected void doInitPresentation(IWidgetPresentation<?> presentation) {
		super.doInitPresentation(presentation);

		// initialize the presentation with the memento before rendering
		//
		YMemento yMemento = loadMemento(getModel().getMementoId());
		if (yMemento != null) {
			((IMementoHandler) getPresentation()).applyMemento(yMemento);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.EmbeddableEditpart#handleModelSet(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case CxGridPackage.CX_GRID__SELECTION_TYPE:
			if (isRendered()) {
				getPresentation().updateSelectionType();
			}
			break;
		case CxGridPackage.CX_GRID__FILTERING_VISIBLE:
			if (isRendered()) {
				getPresentation().updateFilter();
			}
			break;
		case CxGridPackage.CX_GRID__CUSTOM_FILTERS:
			if (isRendered()) {
				getPresentation().updateFilter();
			}
			break;
		case CxGridPackage.CX_GRID__CELL_STYLE_GENERATOR:
			if (isRendered()) {
				getPresentation().updateCellStyleGenerator();
			}
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelAdd(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelAdd(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case CxGridPackage.CX_GRID__COLUMNS:
			if (isRendered()) {
				getPresentation().updateColumns();
			}
			break;
		case CxGridPackage.CX_GRID__HEADERS:
			if (isRendered()) {
				getPresentation().updateHeader();
			}
			break;
		case CxGridPackage.CX_GRID__FOOTERS:
			if (isRendered()) {
				getPresentation().updateFooter();
			}
			break;
		default:
			super.handleModelAdd(featureId, notification);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelAddMany(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelAddMany(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case CxGridPackage.CX_GRID__COLUMNS:
			if (isRendered()) {
				getPresentation().updateColumns();
			}
			break;
		case CxGridPackage.CX_GRID__HEADERS:
			if (isRendered()) {
				getPresentation().updateHeader();
			}
			break;
		case CxGridPackage.CX_GRID__FOOTERS:
			if (isRendered()) {
				getPresentation().updateFooter();
			}
			break;
		default:
			super.handleModelAddMany(featureId, notification);
		}

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelRemove(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelRemove(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case CxGridPackage.CX_GRID__COLUMNS:
			if (isRendered()) {
				getPresentation().updateColumns();
			}
			break;
		case CxGridPackage.CX_GRID__HEADERS:
			if (isRendered()) {
				getPresentation().updateHeader();
			}
			break;
		case CxGridPackage.CX_GRID__FOOTERS:
			if (isRendered()) {
				getPresentation().updateFooter();
			}
			break;
		default:
			super.handleModelRemove(featureId, notification);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelRemoveMany(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelRemoveMany(int featureId,
			Notification notification) {
		checkDisposed();

		switch (featureId) {
		case CxGridPackage.CX_GRID__COLUMNS:
			if (isRendered()) {
				getPresentation().updateColumns();
			}
			break;
		case CxGridPackage.CX_GRID__HEADERS:
			if (isRendered()) {
				getPresentation().updateHeader();
			}
			break;
		case CxGridPackage.CX_GRID__FOOTERS:
			if (isRendered()) {
				getPresentation().updateFooter();
			}
		default:
			super.handleModelRemove(featureId, notification);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.EmbeddableEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {

			getModel().eAdapters().remove(gridContentAdapter);
			gridContentAdapter = null;

		} finally {
			super.internalDispose();
		}
	}
}