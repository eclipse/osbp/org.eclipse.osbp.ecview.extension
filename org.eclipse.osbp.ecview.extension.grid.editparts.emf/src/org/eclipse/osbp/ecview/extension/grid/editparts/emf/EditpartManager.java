/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.editparts.emf;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.common.AbstractEditpartManager;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridColumn;
import org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridBlobImageRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridBooleanRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridButtonRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridDateRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridDelegateRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridHtmlRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridImageRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridIndicatorRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridNumberRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridPriceRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridProgressbarRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridQuantityRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridTextRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBooleanRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDelegateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridHtmlRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridTextRenderer;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Component;

/**
 * An implementation of IEditPartManager for eObjects with
 * nsURI=http://eclipse.org/emf/emfclient/uimodel.
 */
@Component(immediate = true, service = { IEditPartManager.class })
public class EditpartManager extends AbstractEditpartManager {

	/**
	 * Activate.
	 *
	 * @param context
	 *            the context
	 */
	protected void activate(ComponentContext context) {

	}

	/**
	 * Deactivate.
	 *
	 * @param context
	 *            the context
	 */
	protected void deactivate(ComponentContext context) {

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IEditPartManager#isFor(java.lang.Object)
	 */
	@Override
	public boolean isFor(Object element) {
		if (element instanceof EObject) {
			String uriString = ((EObject) element).eClass().getEPackage()
					.getNsURI();
			return uriString.equals(CxGridPackage.eNS_URI)
					|| uriString.equals(CxGridRendererPackage.eNS_URI);
		} else if (element instanceof String) {
			return element.equals(CxGridPackage.eNS_URI)
					|| element.equals(CxGridRendererPackage.eNS_URI);
		}
		return false;
	}

	/**
	 * Creates a new instance of the edit part.
	 * 
	 * @param <A>
	 *            an instance of {@link IElementEditpart}
	 * @param yElement
	 *            the model element
	 * @return editpart
	 */
	@SuppressWarnings("unchecked")
	protected <A extends IElementEditpart> A createEditpart(IViewContext context, Object yElement) {
		// asserts that no editpart was created already for the given element
		assertOneEditpartForModelelement(yElement);

		ElementEditpart<YElement> result = null;
		if (yElement instanceof CxGrid) {
			result = createNewInstance(GridEditpart.class);
		} else if (yElement instanceof CxGridColumn) {
			result = createNewInstance(GridColumnEditpart.class);
		} else if (yElement instanceof CxGridDelegateCellStyleGenerator) {
			result = createNewInstance(GridDelegateCellStyleGeneratorEditpart.class);
		} else if (yElement instanceof CxGridHeaderRow) {
			result = createNewInstance(GridHeaderRowEditpart.class);
		} else if (yElement instanceof CxGridFooterRow) {
			result = createNewInstance(GridFooterRowEditpart.class);
		} else if (yElement instanceof CxGridMetaCell) {
			result = createNewInstance(GridMetaCellEditpart.class);
		} else if (yElement instanceof CxGridDateRenderer) {
			result = createNewInstance(GridDateRendererEditpart.class);
		} else if (yElement instanceof CxGridButtonRenderer) {
			result = createNewInstance(GridButtonRendererEditpart.class);
		} else if (yElement instanceof CxGridHtmlRenderer) {
			result = createNewInstance(GridHtmlRendererEditpart.class);
		} else if (yElement instanceof CxGridImageRenderer) {
			result = createNewInstance(GridImageRendererEditpart.class);
		} else if (yElement instanceof CxGridBlobImageRenderer) {
			result = createNewInstance(GridBlobImageRendererEditpart.class);
		} else if (yElement instanceof CxGridNumberRenderer) {
			result = createNewInstance(GridNumberRendererEditpart.class);
		} else if (yElement instanceof CxGridProgressBarRenderer) {
			result = createNewInstance(GridProgressbarRendererEditpart.class);
		} else if (yElement instanceof CxGridTextRenderer) {
			result = createNewInstance(GridTextRendererEditpart.class);
		} else if (yElement instanceof CxGridDelegateRenderer) {
			result = createNewInstance(GridDelegateRendererEditpart.class);
		} else if (yElement instanceof CxGridBooleanRenderer) {
			result = createNewInstance(GridBooleanRendererEditpart.class);
		} else if (yElement instanceof CxGridQuantityRenderer) {
			result = createNewInstance(GridQuantityRendererEditpart.class);
		} else if (yElement instanceof CxGridPriceRenderer) {
			result = createNewInstance(GridPriceRendererEditpart.class);
		} else if (yElement instanceof CxGridIndicatorRenderer) {
			result = createNewInstance(GridIndicatorRendererEditpart.class);
		} else if (yElement instanceof CxGridNestedConverter) {
			result = createNewInstance(GridNestedPropertyConverterEditpart.class);
		}

		if (result != null) {
			result.initialize(context,(YElement) yElement);
		}

		return (A) result;
	}

	/**
	 * Returns a new instance of the type.
	 * 
	 * @param type
	 *            The type of the editpart for which an instance should be
	 *            created.
	 * @return editPart
	 * @throws InstantiationException
	 *             e
	 * @throws IllegalAccessException
	 *             e
	 */
	protected IElementEditpart newInstance(
			Class<? extends IElementEditpart> type)
			throws InstantiationException, IllegalAccessException {
		try {
			Constructor<? extends IElementEditpart> c = type
					.getDeclaredConstructor(new Class<?>[0]);
			if (!c.isAccessible()) {
				c.setAccessible(true);
				return c.newInstance(new Object[0]);
			}
		} catch (NoSuchMethodException e) {
			throw new IllegalStateException(e);
		} catch (SecurityException e) {
			throw new IllegalStateException(e);
		} catch (IllegalArgumentException e) {
			throw new IllegalStateException(e);
		} catch (InvocationTargetException e) {
			throw new IllegalStateException(e);
		}

		return type.newInstance();
	}
}
