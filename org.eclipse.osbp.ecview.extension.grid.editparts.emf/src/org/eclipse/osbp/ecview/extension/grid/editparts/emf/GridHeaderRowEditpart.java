/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.editparts.emf;

import org.eclipse.osbp.ecview.extension.grid.editparts.IGridHeaderRowEditpart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GridHeaderRowEditpart.
 */
public class GridHeaderRowEditpart extends GridMetaRowEditpart implements
		IGridHeaderRowEditpart {

	/** The Constant LOGGER. */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GridHeaderRowEditpart.class);

	/**
	 * Instantiates a new grid header row editpart.
	 */
	protected GridHeaderRowEditpart() {

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#createModel()
	 */
}
