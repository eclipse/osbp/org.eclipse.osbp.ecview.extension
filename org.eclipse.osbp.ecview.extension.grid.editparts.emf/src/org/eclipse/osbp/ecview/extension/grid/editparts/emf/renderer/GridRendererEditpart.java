/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.extension.grid.editparts.renderer.IGridRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.renderer.IGridRendererRefreshHandler;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The implementation of the IOpenDialogCommandEditpart.
 */
public abstract class GridRendererEditpart<M extends CxGridRenderer> extends
		ElementEditpart<M> implements IGridRendererEditpart {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GridRendererEditpart.class);

	@Override
	public Object createDelegate() {
		return internalCreateDelegate();
	}

	/**
	 * Tells its container, that the renderer needs to become refreshed.
	 */
	protected void notifyRefreshHandler() {
		CxGridRenderer cxRenderer = getModel();
		YElement container = (YElement) cxRenderer.eContainer();
		IElementEditpart containerEp = getEditpart(viewContext, container);
		if (containerEp instanceof IGridRendererRefreshHandler) {
			((IGridRendererRefreshHandler) containerEp).refreshRenderer();
		}
	}

	/**
	 * Resets the delegate and notifies the parent. So it will be instantiated
	 * again at next request.
	 */
	protected void markDirty() {
		LOGGER.debug("Renderer is marked as dirty: " + getModel());

		notifyRefreshHandler();
	}

	protected abstract Object internalCreateDelegate();

}
