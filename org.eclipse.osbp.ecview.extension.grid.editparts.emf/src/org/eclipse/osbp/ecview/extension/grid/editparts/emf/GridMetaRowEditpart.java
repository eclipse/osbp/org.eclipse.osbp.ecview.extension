/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.editparts.emf;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridMetaRowEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.presentation.IGridPresentation;
import org.eclipse.osbp.ecview.extension.grid.util.CxGridUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GridMetaRowEditpart.
 */
public abstract class GridMetaRowEditpart extends
		ElementEditpart<CxGridMetaRow> implements IGridMetaRowEditpart {

	/** The Constant LOGGER. */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GridMetaRowEditpart.class);

	/** The cx grid. */
	private CxGrid cxGrid;

	/**
	 * Instantiates a new grid meta row editpart.
	 */
	protected GridMetaRowEditpart() {

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#initialize(org.eclipse.osbp.ecview.core.common.model.core.YElement)
	 */
	@Override
	public void initialize(IViewContext context, CxGridMetaRow model) {
		super.initialize(context, model);

		cxGrid = CxGridUtil.getGrid(model);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelAdd(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelAdd(int featureId, Notification notification) {
		switch (featureId) {
		case CxGridPackage.CX_GRID_META_ROW__CUSTOM_CELLS:
			updateCustomCellsPresentation();
			break;
		case CxGridPackage.CX_GRID_META_ROW__GROUPINGS:
			updateRowsPresentation();
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelRemove(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelRemove(int featureId, Notification notification) {
		switch (featureId) {
		case CxGridPackage.CX_GRID_META_ROW__CUSTOM_CELLS:
			updateCustomCellsPresentation();
			break;
		case CxGridPackage.CX_GRID_META_ROW__GROUPINGS:
			updateRowsPresentation();
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}

	/**
	 * Update custom cells presentation.
	 */
	protected void updateCustomCellsPresentation() {
		IGridPresentation<?> presentation = GridEditpart.getGridPresentation(
				viewContext, getModel());
		if (presentation != null) {
			presentation.updateColumns();
		}
	}

	/**
	 * Update rows presentation.
	 */
	protected void updateRowsPresentation() {
		IGridPresentation<?> presentation = GridEditpart.getGridPresentation(
				viewContext, cxGrid);
		if (presentation != null) {
			if (getModel() instanceof CxGridHeaderRow) {
				presentation.updateHeader();
			} else if (getModel() instanceof CxGridFooterRow) {
				presentation.updateFooter();
			} else {
				throw new IllegalArgumentException("Not a supported option!");
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			cxGrid = null;
		} finally {
			super.internalDispose();
		}
	}
}
