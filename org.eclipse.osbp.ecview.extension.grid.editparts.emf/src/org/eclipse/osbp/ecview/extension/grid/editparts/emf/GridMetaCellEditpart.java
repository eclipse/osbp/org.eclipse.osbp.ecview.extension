/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.editparts.emf;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.util.emf.ModelUtil;
import org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridMetaCellEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.presentation.IGridPresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GridMetaCellEditpart.
 */
public class GridMetaCellEditpart extends ElementEditpart<CxGridMetaCell>
		implements IGridMetaCellEditpart {

	/** The Constant LOGGER. */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GridMetaCellEditpart.class);

	/**
	 * Instantiates a new grid meta cell editpart.
	 */
	protected GridMetaCellEditpart() {

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#createModel()
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		switch (featureId) {
		case CxGridPackage.CX_GRID_META_CELL__ELEMENT:
			disposeElement((YEmbeddable) notification.getOldValue());
		case CxGridPackage.CX_GRID_META_CELL__LABEL:
		case CxGridPackage.CX_GRID_META_CELL__LABEL_I1_8N_KEY:
		case CxGridPackage.CX_GRID_META_CELL__USE_HTML:
			IGridPresentation<?> presentation = GridEditpart
					.getGridPresentation(viewContext, getModel());
			if (presentation != null) {
				CxGridMetaRow parent = (CxGridMetaRow) getModel().eContainer();
				if (parent instanceof CxGridHeaderRow) {
					presentation.updateHeader();
				} else if (parent instanceof CxGridFooterRow) {
					presentation.updateFooter();
				} else {
					throw new IllegalArgumentException(
							"Not a supported option!");
				}
			}
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}
	
	/**
	 * Dispose element.
	 *
	 * @param yEmbeddable
	 *            the y embeddable
	 */
	protected void disposeElement(YEmbeddable yEmbeddable) {
		IEmbeddableEditpart ep = ModelUtil.findEditpart(yEmbeddable);
		if (ep != null) {
			ep.dispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			disposeElement(getModel().getElement());
		} finally {
			super.internalDispose();
		}
	}
}
